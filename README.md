# JavaScript Course


## Starting

1. Install [nodejs](http://nodejs.org/). For example, in OSX with Homebrew installed you can run `brew install node`
2. Install [grunt-cli](https://github.com/gruntjs/grunt-cli). Run `npm install -g grunt-cli`
3. Clone the repo
4. In the root folder, run `npm install`. This will install all required dependencies.

## Validation

Run `grunt practice:functions`. You should get something like:
```
$ grunt practice:functions

// Massive stack trace here

PhantomJS 1.9.7 (Mac OS X): Executed 47 of 47 (47 FAILED) ERROR (0.021 secs / 0.004 secs)
```


## Run the slides

Run `grunt slides`. Navigate to [http://localhost:8001/](http://localhost:8001/)


## Run the practice

### Practice for 'Functions'
Run `grunt practice:functions`

### Practice for 'Inheritance'
Run `grunt practice:inheritance`

### Practice for 'Types'
Run `grunt practice:types:typeof` for the first practice.

Run `grunt practice:types:schema` for the second practice

### Practice for 'DOM'
Run `grunt practice:dom`, and read src/practices/dom/README.md