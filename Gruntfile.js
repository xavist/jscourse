/* global module:false */
module.exports = function (grunt) {

  var testUtils = [
    "node_modules/chai/chai.js",
    "node_modules/sinon/pkg/sinon.js",
  ];

  // Project configuration
  grunt.initConfig({
    xpkg: grunt.file.readJSON('package.json'),

    connect: {
      slides: {
        options: {
          hostname: '0.0.0.0',
          port: 8001,
          base: 'src/slides',
          keepalive: true,
          middleware: function (connect, options) {
            return [
              connect.static(__dirname + '/src/public'),
              connect.static(__dirname + '/src/slides'),
              connect.static(__dirname + '/node_modules/reveal.js')
            ];
          }
        }
      },

      dom: {
        options: {
          hostname: '0.0.0.0',
          port: 8002,
          base: 'src/practices/dom',
          keepalive: true
        }
      }
    },

    karma: {
      options: {
        frameworks: ["mocha"],
        reporters: ['spec'],
        browsers: [ "PhantomJS" ],
        autoWatch: true
      },

      types1: {
        options: {
          files: testUtils.concat([
            'src/practices/types/typeof.js',
            'src/practices/types/spec/typeof.js'
          ])
        }
      },

      types2: {
        options: {
          files: testUtils.concat([
            'src/practices/types/typeof.js',
            'src/practices/types/schema.js',
            'src/practices/types/spec/schema.js'
          ])
        }
      },

      functions: {
        options: {
          files: testUtils.concat([
            'src/practices/functions/Functional.js',
            'src/practices/functions/spec/Functional.js'
          ])
        }
      },

      inheritance: {
        options: {
          files: testUtils.concat([
            'src/practices/inheritance/Class.js',
            'src/practices/inheritance/spec/Class.js'
          ])
        }
      }
    }
  });

  // Dependencies
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-karma');

  // Serve presentation locally
  grunt.registerTask('slides', [ 'connect:slides']);
  grunt.registerTask('practice:functions', [ 'karma:functions']);
  grunt.registerTask('practice:inheritance', [ 'karma:inheritance']);
  grunt.registerTask('practice:types:typeof', [ 'karma:types1']);
  grunt.registerTask('practice:types:schema', [ 'karma:types2']);
  grunt.registerTask('practice:dom', [ 'connect:dom']);
};
