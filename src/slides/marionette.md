# JavaScript Course
## Backbone + Marionette

<!-- .slide: data-slide-type="header" -->

---
# Backbone + Marionette

<!-- .slide: data-slide-type="group-header" -->

--
# Backbone.js

Library that gives structure to web applications. Provides MVC components:

### Events 
Event system for all the objects

### Models
Stores the data for your application

### Collections
Group of Models

### Views
Representation one or more Models

--
# Backbone.Model

Design:

* Provide sensible defaults
* Extend & override to customize behaviour


--
# Marionette.js

Collection of common design and implementation patterns for Backbone.js

### Object 
Generic object with events and initialization cycle

### Views
Extends Backbone.View. Provides nested views, composition, regions...


---
# Backbone.Event

<!-- .slide: data-slide-type="group-header" -->

--
# Backbone.Event

Common to all Backbone classes

--
# Backbone.Event
## Trigger events

```javascript
//fileUploader extends Backbone.Event

fileUploader.upload = function(file) {
    //... upload the file
    
    //Trigger the event
    this.trigger("fileUpload");
}
```

--
# Backbone.Event
## Listen to events

```javascript
fileUploader.on("fileUpload", function() {
    alert("done!");
});
```

--
# Backbone.Event
## Passing arguments

```javascript
fileUploader.upload = function(file) {
    //Trigger the event
    this.trigger("fileUpload", file, Date.now() );
}

fileUploader.on("fileUpload", function(file, timestamp) {
    alert(file, timestamp);
}
```

--
# Backbone.Event
## Listen to events (II)

A `Backbone.Event` instance listening to another `Backbone.Event` instance

```javascript
//logger extends Backbone.Event

logger.log = function() {
    this.listenTo(fileUploader, "fileUpload", function() { /* ... */})

    // equivalent to
    // fileUploader.on("fileUpload", function() { /* ... */})
}
```

--
# Backbone.Event
## Listen to events (III)

Listen to multiple events

```javascript

logger.log = function() {
    this.listenTo(fileUploader, {
        "fileUpload": function() { /* ... */ },    
        "uploadStart": function() { /* ... */ },
        "uploadCancel uploadError": function() { /* ... */ }    
    })
});
```

--
# Backbone.Event
## Tips & Conventions

Fire events after the action <!-- .element: class="tip" -->

BAD
```javascript
fileUploader.upload = function(file) {
    this.trigger("fileUpload");
    FileUploaderService.send(file)    
}
```

GOOD
```javascript
fileUploader.upload = function(file) {
    FileUploaderService.send(file)
    this.trigger("fileUpload");
}
```

--
# Backbone.Event
## Tips & Conventions

Use present tense <!-- .element: class="tip" -->

BAD
```javascript
fileUploader.upload = function(file) {
    this.trigger("fileUploaded");
}
```

GOOD
```javascript
//Good
fileUploader.upload = function(file) {
    this.trigger("fileUpload");
}
```

--
# Backbone.Event
## Tips & Conventions

Do not pass multiple arguments, pass an object instead  <!-- .element: class="tip" -->

```javascript
fileUploader.upload = function(file) {
    //Trigger the event
    this.trigger("fileUpload", {
        file: file,
        timestamp: Date.now()
    })
}

fileUploader.on("fileUpload", function(ev) {
    alert(ev.file, ev.timestamp);
}
```

--
# Backbone.Event
## Tips & Conventions

Do not listen to your own events. That is a bad architecture <!-- .element: class="tip" -->

--
# Backbone.Event
## Tips & Conventions

Only <!-- .element: class="tip" --> the creator of `X` should listen to events from `X` <!-- .element: class="tip" -->

--
# Backbone.Event
## Tips & Conventions

Prefer <!-- .element: class="tip" --> `listenTo` over `on`


---
# Backbone.Model

<!-- .slide: data-slide-type="group-header" -->

--
# Backbone.Model

Provides data management for your MVC application:

* Store values
* Validators
* Retrieve values from the server
* Filtering
* ...

--
# Backbone.Model
## Attributes

A model is collection of attributes:

```javascript
var issue = new Backbone.Model();

issue.set('key', 'KEY-123');
issue.set('id', 10001);
issue.set('properties', { 
    summary: 'title here',
    description: 'An issue'
})

issue.get('key') //KEY-123

issue.has('summary') //false
```


--
# Backbone.Model
## Extension

You can add any custom method
```javascript
var Issue = Backbone.Model.extend({
    //Your methods here
    setResolution: function(resolution) {
        this.set("status", "Done");
        this.set("resolution", resolution);
    }
})

var issue = new Issue();
issue.setResolution("Fixed");
```

--
# Backbone.Model
## Initialization

`initialize` is the constructor of the model
```javascript
var Issue = Backbone.Model.extend({
    initialize: function() {
        console.debug("Issue created");
        this.set("creation", Date.now());
    }
})

var issue = new Issue();
```

--
# Backbone.Model
## Fetch data form the server 

```javascript
var Issue = Backbone.Model.extend({
    urlRoot: "/jira/rest/api/latest/issue/" 
})

var issue = new Issue({
    id: 10001
});

// Async GET request to "/jira/rest/api/latest/issue/10001"
issue.fetch().done(function() {    
    console.log(issue.toJSON());  
});
```

--
# Backbone.Model
## Transform data to the model representation

`parse` transforms AJAX data into the model representation.

```javascript
var Issue = Backbone.Model.extend({
    urlRoot: "/jira/rest/api/latest/issue/",
    parse: function(rawData) {
        var attributes = {};
        attributes.id = rawData.id;
        attributes.key = rawData.key;
        attributes.summary = rawData.fields.summary;
        return attributes;
    }
})

var issue = new Issue({id: 10001});
// Async GET request to "/jira/rest/api/latest/issue/10001"
issue.fetch().done(function() {    
    console.log(issue.get('summary')); //The summary  
    console.log(issue.has('fields')); //false
});
```

--
# Backbone.Model
## Data binding 

```javascript
// Our model
var Issue = Backbone.Model.extend({
    urlRoot: "/jira/rest/api/latest/issue/"
});
var issue = new Issue({
    id: 10001,
    summary: 'This is my issue'
});

//Render the summary inside a <h1> tag
$("h1").text(issue.get("summary"));

//When the 'name' changes, updates the <h1> tag with the new name
issue.on("change:summary", function() {
    $("h1").text(issue.get("summary"));
});

// Change the name --> this will update the <h1>
issue.set("summary", "This is the new summary");

// Fetching data from the server could update <h1> as well
issue.fetch();
```

--
# Backbone.Model
## Saving data

Not as easy as `parse` :(
```javascript
// Our model
var Issue = Backbone.Model.extend({
    urlRoot: "/jira/rest/api/latest/issue/",
    saveModel: function() {
        this.save(null, { "attrs": {
            "fields": {
                "summary": this.get('summary')
            }
        }});
    }
});
var issue = new Issue({
    id: 10001
});

// Change the name --> this will update the <h1>
issue.set("summary", "This is the new summary");

// Fetching data from the server could update <h1> as well
issue.saveModel();
```


---
# Backbone.Collection

<!-- .slide: data-slide-type="group-header" -->

--
# Backbone.Collection

Provides a collection of Models

--
# Backbone.Collection
## Creation

```javascript
var Issues = Backbone.Collection.extend({
    model: Issue
})

var allIssues = new Issues([
    {id: 10001, key: "DEMO-1"},
    {id: 10002, key: "DEMO-2"}
]);
allIssues.add(new Issue({id: 10003, key: "DEMO-3"}));
allIssues.add({id: 10004, key: "DEMO-3"});
```

--
# Backbone.Collection
## Filtering

```javascript
// By attribute
allIssues.where({key: "DEMO-2"});     //Returns an array
allIssues.findWhere({key: "DEMO-2"}); //Returns an item

// By function
allIssues.filter(function(issue) {    //Returns an array
    return issue.get('id') < 10003;    
});
allIssues.find(function(issue) {      //Returns an item
    return issue.get('id') < 10003;    
});
```

--
# Backbone.Collection
## Indexing

```javascript
// Get a model by model.id
allIssues.get(10003);

// Get a model by postion
allIssues.at(1);
```

--
# Backbone.Collection
## Fetch data form the server 

Like `Backbone.Model`

```javascript
var Issues = Backbone.Collection.extend({
    model: Issue,
    url: "/jira/rest/api/latest/search",
});

var issues = new Issues();

// Async GET request
issues.fetch();
```

--
# Backbone.Collection
## Parse data form the server 

Like `Backbone.Model`

```javascript
var Issues = Backbone.Collection.extend({
    model: Issue,
    url: "/jira/rest/api/latest/search",
    parse: function(rawData) {
        return rawData.issues;
    }
});

var issues = new Issues();

// Async GET request
issues.fetch().done(function() {
    console.log(issues.length);
});
```

--
# Backbone.Collection
## Parse data form the server

Like `Backbone.Model`

```javascript
var Issue = Backbone.Model.extend({
    parse: function(data) {
        return {
            summary: data.fields.summary,
            key: data.key,
            id: data.id
        }
    }
});
var Issues = Backbone.Collection.extend({
    model: Issue
    url: "/jira/rest/api/latest/search",
    parse: function(rawData) {
        return rawData.issues;
    }
});

var issues = new Issues();

// Async GET request
issues.fetch().done(function() {
    issues.findWhere({key: 'QA-20'}).get('summary');
});
```

--
# Backbone.Collection
## Data binding 

```javascript
var issues = new Issues();

issues.on("add", function(issue) {
    $('table').append(
        '<tr>'+
            '<td id="'+issue.get('id')+'">'+issue.get('summary')+'</td>'+
        '</tr>'
    );
});

issues.on("remove", function(issue) {
    $('table').find('#'+issue.get('id')).remove();
});

issues.on("change:summary", function(model, value) {
    $('table').find('#'+model.get('id')).text(value);
});
```

--
# Backbone.Model & Backbone.Collection
## Tips & Conventions

Provide methods to encapsulate the data <!-- .element: class="tip" -->

BAD
```javascript
var Issue = Backbone.Model.extend({});
var issue = new Issue();

// In other class
if (issue.get('status') === "closed") console.log("Closed");
```

GOOD
```javascript
var Issue = Backbone.Model.extend({
    isClosed: function() { return this.get('status') === "closed" }
});
var issue = new Issue();

// In other class
if (issue.isClosed()) console.log("Closed");
```

---
# Marionette.Object

<!-- .slide: data-slide-type="group-header" -->

--
# Marionette.Object

* A base class which other classes can extend from.

* `Marionette.Object` incorporates many backbone conventions and utilities
like `initialize` and `Backbone.Events`.


--
# Marionette.Object
## Creation

```javascript
var MyModule = Backbone.Object.extend({
    //Your methods here
});
```


--
# Marionette.Object
## Constructor

```javascript
var MyModule = Backbone.Object.extend({
    initialize: function(name) {
        this.name = name;
    }
});

var module = new MyModule("Atlassian");
console.log(module.name);  //"Atlassian"
```

--
# Marionette.Object
## Events

```javascript
var Phone = Backbone.Object.extend({
    call: function(number) {
        this.trigger("calling", number);
    }
});

var phone = new Phone();
phone.on("calling", function() {
    alert("Ring ring!")
})
phone.call("0468440256");
```

--
# Marionette.Object
## `triggerMethod`

* Triggers an event
* And calls a method with the event name

```javascript
var Phone = Backbone.Object.extend({
    call: function(number) {
        this.triggerMethod("calling", number);
            //triggers event 'calling'
            //and calls the method 'onCalling'
    }
});
```

Usage:

* External classes can listen for the event `calling`
* Extending classes can override/implement the method `onCalling()`. Example:

```javascript
var VideoPhone = Phone.extend({
    onCalling: function() { this.turnOnCamera(); }
});
```



--
# Marionette.Object
## Destructor

The method `destroy()`:

* Unbind the events
* Triggers event `before:destroy`
* Invokes `onBeforeDestroy()`

```javascript
var DB = Backbone.Object.extend({
    onBeforeDestroy: function() {
        closeDBConnection();
    }
});

var db = new DB();
db.on("before:destroy", function() {
    console.log("Database is gone!");
})

db.destroy();
```


---
# Marionette.View

<!-- .slide: data-slide-type="group-header" -->

--
# Marionette.View

Base class for all the views, extends from `Backbone.View`

--
# Marionette.View
## Properties

```javascript
var View = Marionette.View.extend({
    tagName: 'div',
    className: 'content'
    id 'myContent',
    attributes: {
        data-content-id: "projects"
    }
});
var view = new View();

view.render()
//Generates
//<div id="#myContent" class="content" data-content-id="projects"></div>
```

--
# Marionette.View
## Templates

```soy
{template JIRA.Templates.Sidebar}
    <section>
        <h1>Issues</h1>
        <p>There are no issues in the system</p>
    </section>
{/template}
```

```javascript
var View = Marionette.View.extend({
    template: JIRA.Templates.Sidebar
});
var view = new View();

view.render()
//Generates a <div> with the content of the template JIRA.Templates.Sidebar
```

--
# Marionette.View
## Elements

```javascript
var View = Marionette.View.extend({
    template: JIRA.Templates.Sidebar
});
var view = new View();

view.render()

// Link to the root element of the view
view.el;

// Link to the jQuery root element of the view
view.$el;

// Link to a jQuery function limited to the view
view.$("h1") //Searchs for H1 inside the view
```

--
# Marionette.View
## Template data

```soy
/**
 * @param summary
 * @param issueKey
 */
{template JIRA.Templates.Issue}
    <span>{issueKey} - {summary}</span>
{/template}
```

```javascript
var View = Marionette.View.extend({
    template: JIRA.Templates.Issue,
    serializeData: function() {
        return {
            issueKey: "KEY-123",
            summary: "Create a class about Marionette"
        }
    }
});
var view = new View();

view.render()
```

--
# Marionette.View
## Template and models

```javascript
// Create the view
var IssueView = Marionette.View.extend({
    template: JIRA.Templates.Issue,
    initialize: function() {
        this.lisentTo(model, "change", this.render);
    }
});

// Create our model instance
var issueModel = new Backbone.Model({
    issueKey: "KEY-123",
    summary: "Create a class about Marionette"
});

// Create the view instance
var issueView = new IssueView({
    model: issueModel
});

view.render();
```


--
# Marionette.View
## UI elements

```javascript
var IssueView = Marionette.View.extend({
    ui: {
        "link": "a",
        "title": "h1"
    }
});
var issueView = new IssueView();
issueView.render();

console.log(issueView.ui.title) //<h1> element
```


--
# Marionette.View
## Events

```javascript
var IssueView = Marionette.View.extend({
    ui: {
        "link": "a",
    },
    events: {
        "click @ui.link": function() {
            alert("Link clicked");
        },
        "mouseover h1": function() {
            alert("Over the title!");
        }
    }
});
```

--
# Marionette.View
## Render cycle

* Trigger `"before:render"` event
* Call method `onBeforeRender`
* Render it
* Trigger `"render"` event
* Call method `onRender`

```javascript
var IssueView = Marionette.View.extend({
    onBeforeRender: function() {
        this.$el.addClass('loading');
    },
    onRender: function() {
        this.$el.removeClass('loading');
    }
});
```


--
# Marionette.View
## Tips & Conventions

External objects *should not* access to the `el` element or the `ui` map.

```javascript
//BAD
var View = Marionette.View.extend({
    ui: { link: "a" },
    template: JIRA.Foo
});
//...
view.ui.link.addClass("active");
view.$el.find("a").addClass("active");
```

```javascript
//GOOD
var View = Marionette.View.extend({
    ui: { link: "a" },
    template: JIRA.Foo,
    activate: function() {
        this.ui.link.addClass("active");
    }
})
//...
view.activate();
```


---
# Marionette.ItemView

<!-- .slide: data-slide-type="group-header" -->


--
# Marionette.ItemView

* Extends from `Marionette.View`
* Renders one single item (usually a Backbone.Model)


---
# Marionette.CollectionView

<!-- .slide: data-slide-type="group-header" -->


--
# Marionette.CollectionView

* Extends from `Marionette.View`
* Renders a collection of items (usually a Backbone.Collection)


--
# Marionette.CollectionView
## childView

Used to specify the view for each item.

```javascript
var List = Marionette.CollectionView.extend({
    // Renders this Collection
    collection: IssuesCollection

    // Each model will be rendered using this view
    childView: IssueView
})
```


--
# Marionette.CollectionView
## emptyView

Used when there are no items in the collection.

```javascript
var List = Marionette.CollectionView.extend({
    // Renders this Collection
    collection: IssuesCollection,

    // Use this view if there are no models in 'IssuesCollection'
    emptyView: EmptyList
})
```


---
# Marionette.CompositeView

<!-- .slide: data-slide-type="group-header" -->


--
# Marionette.CompositeView

* Extends from `Marionette.CollectionView`
* Renders a template *and* a collection of items inside


--
# Marionette.CompositeView
## childViewContainer

```soy
{template JIRA.Templates.IssueList}
<section>
    <h2>Issues</h2>
    <ul></ul>
</section>
{/tempalte}
```

```javascript
var List = Marionette.CompositeView.extend({
    // This is the template for the CompositeView
    template: JIRA.Tempaltes.IssuesList

    // Use this view to render each item in the Collection
    childView: IssueView,

    // Look up this element and render the ChildViews inside
    childViewContainer: "ul"
})
```


---
# Marionette.LayoutView

<!-- .slide: data-slide-type="group-header" -->


--
# Marionette.LayoutView

* Extends from `Marionette.View`
* Renders a template with placeholders (aka `Region`s)

--
# Marionette.LayoutView
## Regions

```soy
{template JIRA.Templates.DetailsView}
    <section>
        <div class="list"></div>
        <div class="editor"></div>
    </section>
{/template}
```

```javascript
var DetailsView = Marionette.LayoutView.extend({
    // This is the template of the LayoutView
    template: JIRA.Templates.DetailsView

    // Export these regions
    regions: {
        "list": ".list",
        "editor": ".editor"
    }
})
var detailsView = new DetailsView();

// This will render 'anotherView' inside the region defined as 'list'
// (i.e. inside '<div class="list"></div>')
detailsView.getRegion('list').show(anotherView);


```

--
# Marionette.View
## Tips & Conventions

* Declare all your DOM elements in the `ui` map.
* A view should only modify things inside its main element.
* Overwrite `onRender`/`onBeforeRender` to customize the render cycle.
* Views should be as logic-less as possible: most of the time, the view just triggers event for the user interactions.

---
# Backbone + Marionette Arquitecture

<!-- .slide: data-slide-type="group-header" -->

--
# Backbone + Marionette Arquitecture
## Overview

![](/resources/architecture.svg)
<!-- .element: style="height: 600px" -->

--
# Backbone + Marionette Arquitecture
## Rules

* Methods for going down, events for going up.
* DO NOT skip layers.
* DO NOT communicate siblings.
* Only the creator of `X` can listen for events from `X`.
* Only the creator of `X` can call methods in `X`.
* Deasign modules like they are going to be used by 3rd party plugins.

--
# Backbone + Marionette Arquitecture
## Templates

* Defined in SOY
* Keep your template logic simple

--
# Backbone + Marionette Arquitecture
## View

### Responsiblity
* Abstract *one* template and the DOM operations
* Transform model data into template data
* Define an API to work with the view
* Trigger events for user interactions

### DON'T
* Don't create subviews
* Don't modify the model
* Don't call any other controller/service
* Don't reuse the view

*Example*: https://stash.atlassian.com/projects/JIRA/repos/jira-projects/browse/jira-projects-issue-navigator/src/main/resources/js/components/simpleissuelist/views/Pagination.js

--
# Backbone + Marionette Arquitecture
## ViewManager/Controller

### Responsiblity
* Manage the lifecycle of views (create and destroy them)
* Compose complex views
* Listen for view events and react properly, usually showing a different view, or rethrowing them
* Expose API to work with the views

### DON'T
* Don't call any other controller/service

*Example*: https://stash.atlassian.com/projects/JIRA/repos/jira-projects/browse/jira-projects-issue-navigator/src/main/resources/js/components/simpleissuelist/controllers/List.js

--
# Backbone + Marionette Arquitecture
## Entities

### Responsiblity
* Expose methods to update the entities in a consistent way
* Trigger events when the data is changed

### DON'T
* Don't have models in a "dirty" state (i.e. having data that has not been saved or validated by the server).
* Call the views or controllers

--
# Backbone + Marionette Arquitecture
## Services

### Responsiblity
* Collection of related functionallity
* Stateless

*Example*: https://stash.atlassian.com/projects/JIRA/repos/jira-projects/browse/jira-projects-issue-navigator/src/main/resources/js/services/Filters.js

--
# Backbone + Marionette Arquitecture
## Module

### Responsiblity
* Main entry point
* Build other modules, view controllers or entities
* Wire things together
* Call services
* Expose a public API (events + methods)

### DON'T
* Trigger analytics events
* Trigger JIRA.traces
* Manage the URL
* Manage keyboard shortcuts

*Example*: https://stash.atlassian.com/projects/JIRA/repos/jira-projects/browse/jira-projects-issue-navigator/src/main/resources/js/SidebarIssueSearch.js

--
# Backbone + Marionette Arquitecture
## Page Application

### Responsiblity
* Compose the page using a module
* Update URL with the state
* Handle Keyboard Shortcuts
* Analitycs
