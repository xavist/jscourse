# JavaScript Course
## Inheritance

<!-- .slide: data-slide-type="header" -->

---
# Prototype

--
# Prototype
## What is an object?

* Collection of key/value properties
* And a link to a prototype

![](http://yuml.me/diagram/scruffy;scale:180/class/[MyObject|key1=value1;key2=value2;...{bg:green}]-.->[Prototype])

--
# Prototype
## What is a prototype?

* An object that contains part of the implementation

* Every object has a prototype. It can be:
  * Any other object
  * or `null`

![](http://yuml.me/diagram/scruffy;scale:180/class/[MyObject|key1=value1;key2=value2;...]-.->[Prototype{bg:green}])

--
# Prototype
## Creating the Prototype chain

Use `Object.create()`

--
# Prototype
## Creating the Prototype chain

Let's say `employee` is the prototype of `developer`

![](http://yuml.me/diagram/scruffy;scale:180/class/[developer|name=“Charlie”;age=42]-.->[employee|company=“Atlassian”])

```javascript
var employee = {
    company: "Atlassian"
};

var developer = Object.create(employee)
developer.name = "Charlie"
developer.age = 42
```

--
# Prototype
## See it in action

![](http://yuml.me/diagram/scruffy;scale:180/class/[developer|name=“Charlie”;age=42]-.->[employee|company=“Atlassian”])

```javascript
console.log( developer.name )    //"Charlie"
console.log( developer.company ) //"Atlassian"
```

When we access a property:

1. Search for it in the current object
2. If found, just return the value
3. If not found, check the object's prototype
4. Repeat until we reach the end of the prorotype chain

--
# Prototype
## Multi level

![](http://yuml.me/diagram/scruffy;scale:180;dir:LR;/class/[developer|lang=“JS”]-.->[employee|location=“Sydney”],[employee]-.->[person|age=42],[person]-.->[...])

Works with multiple levels

```javascript
developer.lang     //"JS"
developer.location //"Sydney"
developer.age      //42
```

--
# Prototype
## Real time

![](http://yuml.me/diagram/scruffy;scale:180/class/[developer|name=“Charlie”;age=42]-.->[employee|company=“Atlassian”])

Properties are evaluated in real-time

```javascript
console.log( developer.company )  //"Atlassian"

employee.location = "Sydney"

console.log( developer.location ) //"Sydney"
```



--
# Prototype
## End of the chain

![](http://yuml.me/diagram/scruffy;scale:180/class/[person]-.->[Object.prototype{bg:green}],[Object.prototype{bg:green}]-.->[null{bg:red}])

* The end of the chain is always "null"
* All objects has `Object.prototype` as prototype


```javascript
var person = {}

Object.prototype.sayHello = function() {console.log("Hello!")}
person.sayHello()  //"Hello!"
```

--
# Prototype
## `this` is not related with the prototype!

![](http://yuml.me/diagram/scruffy;scale:120/class/[mary|name=“Mary”]-.->[Person|greet=function%28%29;name=“random%20stranger”],[paul|name=“Paul”]-.->[Person])

```javascript
// (prototype creation omitted for clarity)

var person = {
    greet: function() { return "Hello, I'm " + this.name; },
    name: "random stranger"
};
var mary = { name: "Mary" };
var paul = { name: "Paul" };

person.greet(); //>"Hello, I'm random stranger"
mary.greet();   //>"Hello, I'm Mary"
paul.greet();   //>"Hello, I'm Paul"
```

Remember: when calling a function using `a.b()`, inside the function `this` is `a`

--
# Prototype
## Modify vs. Write

![](http://yuml.me/diagram/scruffy;scale:180/class/[admin]-.->[person|langs=array%28%29],[charlie]-.->[person])

--
# Prototype
## Modify vs. Write

When *modifying* an `object`, it is modified in the prototype

```javascript
admin.langs.push("JS")

charlie.langs.length   //1
admin.langs.length     //1
person.langs.length    //1
```

![](http://yuml.me/diagram/scruffy;scale:180/class/[admin]-.->[person|langs=array%28“JS”%29],[charlie]-.->[person])

--
# Prototype
## Modify vs. Write

When *writing* any value, it is always saved in the current object

```javascript
admin.langs = ["JS"];

charlie.langs.length  //0
admin.langs.length    //1
person.langs.length   //0
```

![](http://yuml.me/diagram/scruffy;scale:180/class/[admin|langs=array%28“JS”%29]-.->[person|langs=array%28%29],[charlie]-.->[person])



---
# Clases & Instances

--
# Clases & Instances
## Created by functions

Classes are simulated with functions:

1. Define a `function`
2. Set the `prototype` property
2. Use `new` operator

--
# Clases & Instances
## Define a `function`

Store any object in the `prototype` property

```javascript
var Person = function(){};
```

--
# Clases & Instances
## Set the `prototype` property

Store any object in the `prototype` property

```javascript
var Person = function(){};

Person.prototype = { sayHello: function() {return "Hello!"} }
```

--
# Clases & Instances
## Use `new` operator

Call that function using `new`

```javascript
var Person = function(){}

Person.prototype = { sayHello: function() {return "Hello!"} }

var charlie = new Person()
charlie.sayHello() //"Hello!"
```

Now `charlie` has `Person.prototype` as its prototype

--
# Clases & Instances
## `new` in detail

The `new f()` operator

1. Executes the function `f`
2. Inside the function, the value of `this` is an empty object
3. The prototype of `this` is `f.prototype`
4. `this` is returned by default

```javascript
function F(){
    // `this` is an empty object
    JSON.stringify(this) === "{}"

    // `myObject` is the prototype of `this`
    myObject.isPrototypeOf(this) === true

    // `this` is returned by default
    // return this
}
F.prototype = myObject
```

--
# Clases & Instances
## Function acts as a constructor

```javascript
function Developer(name) {
    this.name = name
}
Developer.prototype = {
    sayHello: function(){
        return "Hello!, I'am " + this.name
    }
};

var anna = new Developer('Anna');
anna.name       //"Anna"
anna.sayHello() //"Hello!, I'am Anna"
```

![](http://yuml.me/diagram/scruffy;scale:180/class/[anna|name=“Anna”]-.->[Developer.prototype|sayHello=function%28%29])

--
# Clases & Instances
## Beware of objects in the prototype

```javascript
function Developer() {}
Developer.prototype.skills = {}   // Don't do this

var charlie = new Developer()
charlie.skills.coding = 10;

var bob = new Developer();
console.log (bob.skills.coding) //10
```

* Always define properties *inside* the constructor
* Use the prototype *only* for functions


--
# Clases & Instances
## Default prototype

By default, every function gets a property `prototype` with a default `constructor` property

```javascript
function f() {}
f.prototype.constructor === f //true
```

If you _ovewrite_ the `prototype`, you should restore the `constructor` property
```javascript
function f() {};
f.prototype = {
  ke1: "myValue",
  //...
  constructor: f  //Restore constructor
};
```

---
# Inheritance

* There are lots of patterns to do inheritance in JS.
* Depends on what API you want.

--
# Inheritance
## How to create instances?

```javascript
var instance = new Class();
```

or

```javascript
var instance = Class();
```

or 

```javascript
var instance = Class.create();
```

--
# Inheritance
## How to define the constructor?

```javascript
function Class() {
  //constructor
}
```

or

```javascript
var Class.prototype = {
  constructor: function() {
    //constructor
  }
}
```

--
# Inheritance
## How to access the parent methods?

```javascript
function myMethod() {
  ParentClass.prototype.myMethod.call(this)
}
```

or

```javascript
function myMethod() {
  this.superClass.myMethod()
}
```

or

```javascript
function myMethod() {
  this.super()
}
```

--
# Inheritance
## Other questions?

* Do we support multi-inheritance? (yes, it is possible!)
* Do we support mixins?
* Is the prototype alive? (change in the prototype -> change in all the instances)
* Do you need reflection? (i.e. instanceof )

--
# Inheritance
## Prototypal inheritance

```javascript
// Parent class
function Shape(){};
Shape.prototype.move = function (x, y) {}

// Child class
function Square(){};

// Inheritance
Square.prototype = Object.create(Shape.prototype);

// Child methods
Square.prototype.area = function () {}
Square.prototype.move = function (x, y) {
  Shape.prototype.move.apply(this, arguments);
}

var square = new Square();
```

--
# Inheritance
## Functional inheritance

```javascript
// Parent class
function Shape(){
  return {
    move: function(x,y) {}
  }
};

// Child class
function Square(){
  var shape = Shape();
  var square = Object.create(shape);

  square.area = function() {}
  square.move = function(x, y) { shape.move(x,y) }

  return shape;
};

var square = Square();
```

--
# Inheritance
## Backbone-based

```javascript
// Parent class
var Shape = Backcbone.Model.extend({
  initialize: function() {}
  move: function(x, y) {}
});

// Child class
var Square = Shape.extend({
  area: function() {},
  move: function(x, y) { Shape.prototype.move.apply(this, arguments); }
})

var square = new Square();
```

--
# Inheritance
## jQuery-based

```javascript
// Parent class
function Shape(){};
$.extend(Shape.prototype, {
  move: function(x, y) {}
})

// Child class
var Square = function() {}
$.extend(Square.prototype, Shape.prototpe, {
  area: function() {},
  move: function(x, y) { Shape.prototype.move.apply(this, arguments); }
})

var square = new Square();
```

--
# Inheritance
## Atlassian-based

```javascript
// Parent class
var Shape = Class({
  constructor: function() {},
  move: function(x, y){}
});

// Child class
var Square = Class(Shape, {
  constructor: function() {},
  area: function() {},
  move: function(x, y) { this.super(x, y); }
});

var square = new Square();
```

Does not exists yet. *You* will build it during the practice!


---
# Type detection

--
# Type detection
## `instanceof` operator

```javascript
var a = new Array()
a instanceof Array  //true
a instanceof Object //true
```

Does not work with Functional inheritance

--
# Type detection
## `Object.prototype.isPrototypeOf`

```javascript
var a = new Array()

Array.prototype.isPrototypeOf(a)  //true
Object.prototype.isPrototypeOf(a) //true
```

Does not work with Functional inheritance

--
# QUESTIONS?

--
# CODING TIME!

![](/resources/pairing-time.png)

```bash
$ grunt practice:inheritance
```