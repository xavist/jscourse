# JavaScript Course
## Backbone.JS

<!-- .slide: data-slide-type="header" -->

---
# Overview

<!-- .slide: data-slide-type="group-header" -->

--
# Backbone.js

Library that gives structure to web applications. Provides MVC components:

### Events 
Event system for all the objects

### Models
Stores the data for your application

### Collections
Group of Models

### Views
Representation one or more Models

--
# Backbone.Model

Design:

* Provide sensible defaults
* Extend & override to customize behaviour


---
# Backbone.Event

<!-- .slide: data-slide-type="group-header" -->

--
# Backbone.Event

Common to all Backbone classes

--
# Backbone.Event
## Trigger events

```javascript
//fileUploader extends Backbone.Event

fileUploader.upload = function(file) {
    //... upload the file
    
    //Trigger the event
    this.trigger("fileUpload");
}
```

--
# Backbone.Event
## Listen to events

```javascript
fileUploader.on("fileUpload", function() {
    alert("done!");
});
```

--
# Backbone.Event
## Passing arguments

```javascript
fileUploader.upload = function(file) {
    //Trigger the event
    this.trigger("fileUpload", file, Date.now() );
}

fileUploader.on("fileUpload", function(file, timestamp) {
    alert(file, timestamp);
}
```

--
# Backbone.Event
## Listen to events (II)

A `Backbone.Event` instance listening to another `Backbone.Event` instance

```javascript
//logger extends Backbone.Event

logger.log = function() {
    this.listenTo(fileUploader, "fileUpload", function() { /* ... */})

    // equivalent to
    // fileUploader.on("fileUpload", function() { /* ... */})
}
```

--
# Backbone.Event
## Listen to events (III)

Listen to multiple events

```javascript

logger.log = function() {
    this.listenTo(fileUploader, {
        "fileUpload": function() { /* ... */ },    
        "uploadStart": function() { /* ... */ },
        "uploadCancel uploadError": function() { /* ... */ }    
    })
});
```

--
# Backbone.Event
## Tips & Conventions

Fire events after the action <!-- .element: class="tip" -->

BAD
```javascript
fileUploader.upload = function(file) {
    this.trigger("fileUpload");
    FileUploaderService.send(file)    
}
```

GOOD
```javascript
fileUploader.upload = function(file) {
    FileUploaderService.send(file)
    this.trigger("fileUpload");
}
```

--
# Backbone.Event
## Tips & Conventions

Use present tense <!-- .element: class="tip" -->

BAD
```javascript
fileUploader.upload = function(file) {
    this.trigger("fileUploaded");
}
```

GOOD
```javascript
//Good
fileUploader.upload = function(file) {
    this.trigger("fileUpload");
}
```

--
# Backbone.Event
## Tips & Conventions

Do not pass multiple arguments, pass an object instead  <!-- .element: class="tip" -->

```javascript
fileUploader.upload = function(file) {
    //Trigger the event
    this.trigger("fileUpload", {
        file: file,
        timestamp: Date.now()
    })
}

fileUploader.on("fileUpload", function(ev) {
    alert(ev.file, ev.timestamp);
}
```

--
# Backbone.Event
## Tips & Conventions

Do not listen to your own events. That is a bad architecture <!-- .element: class="tip" -->

--
# Backbone.Event
## Tips & Conventions

Only <!-- .element: class="tip" --> the creator of `X` should listen to events from `X` <!-- .element: class="tip" -->

--
# Backbone.Event
## Tips & Conventions

Prefer <!-- .element: class="tip" --> `listenTo` over `on`


---
# Backbone.Model

<!-- .slide: data-slide-type="group-header" -->

--
# Backbone.Model

Provides data management for your MVC application:

* Store values
* Validators
* Retrieve values from the server
* Filtering
* ...

--
# Backbone.Model
## Attributes

A model is collection of attributes:

```javascript
var issue = new Backbone.Model();

issue.set('key', 'KEY-123');
issue.set('id', 10001);
issue.set('properties', { 
    summary: 'title here',
    description: 'An issue'
})

issue.get('key') //KEY-123

issue.has('summary') //false
```


--
# Backbone.Model
## Extension

You can add any custom method
```javascript
var Issue = Backbone.Model.extend({
    //Your methods here
    setResolution: function(resolution) {
        this.set("status", "Done");
        this.set("resolution", resolution);
    }
})

var issue = new Issue();
issue.setResolution("Fixed");
```

--
# Backbone.Model
## Initialization

`initialize` is the constructor of the model
```javascript
var Issue = Backbone.Model.extend({
    initialize: function() {
        console.debug("Issue created");
        this.set("creation", Date.now());
    }
})

var issue = new Issue();
```

--
# Backbone.Model
## Fetch data form the server 

```javascript
var Issue = Backbone.Model.extend({
    urlRoot: "/jira/rest/api/latest/issue/" 
})

var issue = new Issue({
    id: 10001
});

// Async GET request to "/jira/rest/api/latest/issue/10001"
issue.fetch().done(function() {    
    console.log(issue.toJSON());  
});
```

--
# Backbone.Model
## Transform data to the model representation

`parse` transforms AJAX data into the model representation.

```javascript
var Issue = Backbone.Model.extend({
    urlRoot: "/jira/rest/api/latest/issue/",
    parse: function(rawData) {
        var attributes = {};
        attributes.id = rawData.id;
        attributes.key = rawData.key;
        attributes.summary = rawData.fields.summary;
        return attributes;
    }
})

var issue = new Issue({id: 10001});
// Async GET request to "/jira/rest/api/latest/issue/10001"
issue.fetch().done(function() {    
    console.log(issue.get('summary')); //The summary  
    console.log(issue.has('fields')); //false
});
```

--
# Backbone.Model
## Data binding 

```javascript
// Our model
var Issue = Backbone.Model.extend({
    urlRoot: "/jira/rest/api/latest/issue/"
});
var issue = new Issue({
    id: 10001,
    summary: 'This is my issue'
});

//Render the summary inside a <h1> tag
$("h1").text(issue.get("summary"));

//When the 'name' changes, updates the <h1> tag with the new name
issue.on("change:summary", function() {
    $("h1").text(issue.get("summary"));
});

// Change the name --> this will update the <h1>
issue.set("summary", "This is the new summary");

// Fetching data from the server could update <h1> as well
issue.fetch();
```

--
# Backbone.Model
## Saving data

Not as easy as `parse` :(
```javascript
// Our model
var Issue = Backbone.Model.extend({
    urlRoot: "/jira/rest/api/latest/issue/",
    saveModel: function() {
        this.save(null, { "attrs": {
            "fields": {
                "summary": this.get('summary')
            }
        }});
    }
});
var issue = new Issue({
    id: 10001
});

// Change the name --> this will update the <h1>
issue.set("summary", "This is the new summary");

// Fetching data from the server could update <h1> as well
issue.saveModel();
```


---
# Backbone.Collection

<!-- .slide: data-slide-type="group-header" -->

--
# Backbone.Collection

Provides a collection of Models

--
# Backbone.Collection
## Creation

```javascript
var Issues = Backbone.Collection.extend({
    model: Issue
})

var allIssues = new Issues([
    {id: 10001, key: "DEMO-1"},
    {id: 10002, key: "DEMO-2"}
]);
allIssues.add(new Issue({id: 10003, key: "DEMO-3"}));
allIssues.add({id: 10004, key: "DEMO-3"});
```

--
# Backbone.Collection
## Filtering

```javascript
// By attribute
allIssues.where({key: "DEMO-2"});     //Returns an array
allIssues.findWhere({key: "DEMO-2"}); //Returns an item

// By function
allIssues.filter(function(issue) {    //Returns an array
    return issue.get('id') < 10003;    
});
allIssues.find(function(issue) {      //Returns an item
    return issue.get('id') < 10003;    
});
```

--
# Backbone.Collection
## Indexing

```javascript
// Get a model by model.id
allIssues.get(10003);

// Get a model by postion
allIssues.at(1);
```

--
# Backbone.Collection
## Fetch data form the server 

Like `Backbone.Model`

```javascript
var Issues = Backbone.Collection.extend({
    model: Issue,
    url: "/jira/rest/api/latest/search",
});

var issues = new Issues();

// Async GET request
issues.fetch();
```

--
# Backbone.Collection
## Parse data form the server 

Like `Backbone.Model`

```javascript
var Issues = Backbone.Collection.extend({
    model: Issue,
    url: "/jira/rest/api/latest/search",
    parse: function(rawData) {
        return rawData.issues;
    }
});

var issues = new Issues();

// Async GET request
issues.fetch().done(function() {
    console.log(issues.length);
});
```

--
# Backbone.Collection
## Parse data form the server

Like `Backbone.Model`

```javascript
var Issue = Backbone.Model.extend({
    parse: function(data) {
        return {
            summary: data.fields.summary,
            key: data.key,
            id: data.id
        }
    }
});
var Issues = Backbone.Collection.extend({
    model: Issue
    url: "/jira/rest/api/latest/search",
    parse: function(rawData) {
        return rawData.issues;
    }
});

var issues = new Issues();

// Async GET request
issues.fetch().done(function() {
    issues.findWhere({key: 'QA-20'}).get('summary');
});
```

--
# Backbone.Collection
## Data binding 

```javascript
var issues = new Issues();

issues.on("add", function(issue) {
    $('table').append(
        '<tr>'+
            '<td id="'+issue.get('id')+'">'+issue.get('summary')+'</td>'+
        '</tr>'
    );
});

issues.on("remove", function(issue) {
    $('table').find('#'+issue.get('id')).remove();
});

issues.on("change:summary", function(model, value) {
    $('table').find('#'+model.get('id')).text(value);
});
```

--
# Backbone.Model & Backbone.Collection
## Tips & Conventions

Provide methods to encapsulate the data <!-- .element: class="tip" -->

BAD
```javascript
var Issue = Backbone.Model.extend({});
var issue = new Issue();

// In other class
if (issue.get('status') === "closed") console.log("Closed");
```

GOOD
```javascript
var Issue = Backbone.Model.extend({
    isClosed: function() { return this.get('status') === "closed" }
});
var issue = new Issue();

// In other class
if (issue.isClosed()) console.log("Closed");
```

---
# Backbone.View

<!-- .slide: data-slide-type="group-header" -->

--
# Backbone.View

Renders information about a Model/Collection

--
# Backbone.View
## Defining the container

```
var IssueView = Backbone.View.extend({
    tagName: 'div',
    id: 'issue',
    className: 'aui-block',
    attributes: {
        stye: 'float:left'
    }
})
var view = new IssueView();

view.render();
//Generates <div id='issue' className='aui-block' style='float:left' />
```

--
# Backbone.View
## Adding a view to the page

```
var IssueView = Backbone.View.extend({
    id: 'issue',
})
var view = new IssueView();

$("div.container").append(view.el);
view.render();
```

* `el` points to the root element of the view
* `$el` is the jQuery version of `el` (equivalent to `$(view.el)`)


--
# Backbone.View
## Reusing an existing element

```html
<body><div id='issue'></div></body>
```
```javascript
var IssueView = Backbone.View.extend({
})
var view = new IssueView({
    el: '#issue'
});

//Works with the existing `<div id='issue'></div>`
```

--
# Backbone.View
## Rendering content inside the view

```html
<body><div id='issue'></div></body>
```
```javascript
var IssueView = Backbone.View.extend({
    render: function() {
        this.$el.html("<h1>The title</h1>");
    }
})

var view = new IssueView({
    el: '#issue'
});
view.render();

//The HTML is
//<body><div id='issue'><h1>The title</h1></div></body>
```

--
# Backbone.View
## Rendering a model inside the view

```html
<body><div id='issue'></div></body>
```
```javascript
var IssueView = Backbone.View.extend({
    render: function() {
        this.$el.html("<h1>" + this.model.get('summary') + "</h1>");
    }
})

var issue = new Issue({id: 10001, summary: 'My issue'});
var view = new IssueView(
    model: issue,
    el: '#issue',
);
view.render();

//The HTML is
//<body><div id='issue'><h1>My issue</h1></div></body>
```

--
# Backbone.View
## Using templates

```html
<body><div id='issue'></div></body>
```
```javascript
var IssueView = Backbone.View.extend({
    template: SoyTemplate,
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
    }
})

var issue = new Issue({id: 10001, summary: 'My issue'});
var view = new IssueView(
    model: issue,    
    el: '#issue'
);
view.render();
```

--
# Backbone.View
## Adding events

```javascript
var IssueView = Backbone.View.extend({
    template: SoyTemplate,
    el: '#issue',
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
    },
    events: {
        'click': 'open'
    },
    open: function() {
        var dialog = new JIRAFormDialog().open();
    }
})
```

--
# Backbone.View
## More events

```javascript

    events: {
        'click': 'open',
        'click a.help-link': 'openHelp',
        'mouseover button.submit': function() {
            this.$el.className('active');
        },
    }
})
```

--
# Backbone.View
## Replacing `el` after the view is created

```javascript
var IssueView = Backbone.View.extend({
    template: SoyTemplate,
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
    },
    events: {
        'click': function() {var dialog = new JIRAFormDialog().open();}
    }
});
// Render in memory
view.render();

// Attach to the page
view.setElement('div#container');
```


--
# Backbone.Model
## Data binding with views

```javascript
var IssueModel = Backbone.Model.extend({
    urlRoot: "/jira/rest/api/latest/issue/",
    parse: function(rawData) {
        return { id: rawData.id, summary: rawData.fields.summary };
    }
});

var IssueView = Backbone.View.extend({
    initialize: function() {
        this.listenTo(this.model, 'change:summary', this.render);
    },
    render: function() {
        this.$el.html('<h1>' + this.model.get('summary') + '</h1>');
    }
});

var model = new IssueModel({ id: 10001 });
var view = new IssueView({ model: issue, el: '#issueContainer' });

// Fetching data will update the view
issue.fetch();
```