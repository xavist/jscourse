# JavaScript Course
## DOM

<!-- .slide: data-slide-type="header" -->

---
# Document Object Model

* Is an API for valid HTML and XML documents.
* It defines the logical structure of documents and the way a document is accessed and manipulated.
* Based on NODES and NODELISTS

--
# Document Object Model
## Nodes

Abstract class for all elements in the document

--
# Document Object Model
## Nodes

Types of nodes:

* `Document`
* `Document Fragment`
* `Element`
* `Attribute`
* `Text`
- - -
Others:
CDATA, Entity reference, Entity, Processing Instruction, Comment, Document Type, Notation

--
# Document Object Model
## Nodes

They create a tree structure. Example:

```html
<html>
    <body>
        <div id="content" class="main">MAIN</div>
    </body>
</html>
```

How many nodes are there?

--
# Document Object Model
## Nodes

11 nodes!

![](/resources/dom-nodes.svg)


---
# Document

* It provides info about the page URL.
* It serves as an entry point into the web page's content.
* It is used to search for elements in the document.
* It is used to create new elements in the page.

--
# Document

Each HTML page has one main `document`.

```javascript
//Global variable
document

//Window property
window.document
```

Each iframe in the page has a `document` as well.

```javascript
myIframe.contentDocument
```

--
# Document
## Page URL

The URL of the page is stored in `document.location`
```javascript
console.log(document.location)
//{
//   href: "http://localhost:8001/?class=dom#/3/1",
//   origin: "http://localhost:8001",
//   protocol: "http:",
//   host: "localhost:8001",
//   hash: "#/3/1",
//   ...
//}
```

--
# Document
## Access to content

```javascript
document.documentElement   //Returns the root node (i.e. <html>)
document.head              //<head> node
document.body              //<body> node
```

--
# Document
## Node creation

```javascript
document.create<Type>(value)
```

Examples:
```javascript
var myElement = document.createElement("div")
var myText = document.createTextNode("This is a random text")
var myAttribute document.createAttribute("href")
//and so on
```

* Creates a new node *in memory*
* You need to insert the node into the tree

--
# Document
## Node Selectors

Select one element by ID.
```javascript
document.getElementById("myId")
```

Select one element by CSS selector
```javascript
document.querySelector(".content .aui-form") //Returns the first one
```

--
# Document
## Nodelist Selectors

Select elements by tag

```javascript
document.getElementsByTagName("p") //Use "*" for all tags
```

Select all elements by class name
```javascript
document.getElementsByClassName("myClass")
```

Select all elements by CSS selector
```javascript
document.querySelectorAll(".content .aui-form") //Returns all of them
```


---
# Nodes

* A node belongs to *one* document.
* A node could be detached from the tree.
* A node can only be in *one* position in the tree.

--
# Nodes
## Belongs to a document

Use `ownerDocument` to get the owner document
```javascript
var div = document.createElement("div");

div.ownerDocument === document;  //true
```

--
# Nodes
## Belongs to a document

Use `document.adoptNode` to change the owner

```javascript
var mainDocument = document;
var iframeDocument = myIframe.contentDocument;

//Move an element from the iframe to the main page
document.adoptNode(
    iframeDocument.getElementById('main')
);
```

It *moves* the element from the iframe to the main document.


--
# Nodes
## Belongs to a document

Use `document.importNode` to copy the element into another document

```javascript
var mainDocument = document;
var iframeDocument = myIframe.contentDocument;

//Copy an element from the iframe to the main page
document.importNode(
    iframeDocument.getElementById('main')
);
```

It *copies* the element from the iframe to the main document.

--
# Nodes
## Attributes

All nodes have a name, stored in `nodeName`

```javascript
document.createElement("div").nodeName  //"div"
document.createAttribute("id").nodeName //"id"

//... even if a 'name' makes no sense for a particular node.
document.nodeName  //"#document"
document.createTextNode("content").nodeName //"#text"
```

--
# Nodes
## Attributes

All nodes have a value (`string` or `null`), stored in `nodeValue`

```javascript
document.createTextNode("content").nodeValue //"content"
document.createAttribute("id").nodeValue = "myId"

//... even if a 'value' makes no sense for a particular node.
document.createElement("div").nodeValue  //null
document.nodeValue  //null
```

--
# Nodes
## Attributes

All nodes have a type, stored in `nodeType` as a `number`

|Name                               |Value |
|--------                           |------|
|ELEMENT_NODE                       |1     |
|ATTRIBUTE_NODE                     |2     |
|TEXT_NODE                          |3     |
|CDATA_SECTION_NODE                 |4     |
|ENTITY_REFERENCE_NODE              |5     |
|ENTITY_NODE                        |6     |
|PROCESSING_INSTRUCTION_NODE        |7     |
|COMMENT_NODE                       |8     |
|DOCUMENT_NODE                      |9     |
|DOCUMENT_TYPE_NODE                 |10    |
|DOCUMENT_FRAGMENT_NODE             |11    |
|NOTATION_NODE                      |12    |


--
# Nodes
## Tree navigation

```javascript
Node.firstChild
Node.lastChild
Node.childNodes   //returns a NodeList, includes *all* types of nodes

Node.parentNode

Node.nextSibling
Node.previousSibling

Node.ownerDocument
```

Example:

```javascript
//<div id="test"><p><span>Hello!</span></p></div>

document.getElementById("test").firstChild.firstChild //returns the <span> node
```
Protip: beware of text nodes between tags!

--
# Nodes
## Relationships

`contains` returns `true` if a node is inside another

Example:

```javascript
// Is <body> inside <html>?
document.rootElement.contains(document.body) //true

// Is <head> is inside <body>?
document.body.contains(document.head)        //false
```

--
# Nodes
## Tree modification

```javascript
Node.appendChild(node)
Node.insertBefore(node, target)

Node.removeChild(node)
Node.replaceChild(node)
```

Example:

```javascript
var title = document.createElement("h1")
document.body.appendChild(title)  // Adds a H1 at the end of <body>

document.documentElement.removeChild(doument.head) // Remove the <head>!
```
* Remember, a node can only be in *one* place: appending a node will remove it from the previous position in the tree
* Think of those methods as *move* operations

--
# Nodes
## Copying nodes

```javascript
Node.cloneNode()
```

Example:

```javascript
// Duplicates the first item in a list
var ul = document.querySelector("ul")
var li = ul.firstChild
var liNew = li.cloneNode()
ul.insertBefore(liNew, li)  //Insert liNew before li
```

Warning: be careful with duplicated IDs


---
# Element

* Is a particular type of `Node`
* Represents a &lt;tag&gt;

--
# Element
## Properties

```javascript
Element.tagName
Element.className
Element.id

Element.children     //returns a NodeList, includes *only* Elements
Element.innerHTML    //set/get content as a string
Element.innerText    //get content as text
```

--
# Element
## Properties

Example:

```javascript
// <div id="test" class="aui big">This is a <b>test!!</b></div>

var element = document.getElementById('test')

element.tagName;    //'div'
element.className;  //'aui big'
element.id;         //'test'

element.children;   // [ bElement ]
element.innerHTML;  // 'This is a <b>test!!</b>'
element.innerText;  // 'This is a test!!'
```


--
# Element
## Managing attrbitues

```javascript
Element.getAttribute(name)
Element.removeAttribute(name)
Element.hasAttribute(name)
Element.setAttribute(name, value)
```

or, use Attribute Nodes

--
# Element
## Managing attrbitues

Examples

With methods:

```javascript
// <a href="www.atlassian.com">Atlassian</a>

element.hasAttribute('href') //true
element.setAttribute('href', 'http://www.atlassian.com')
```

With Attribute nodes:

```javascript
var href = document.createAttribute('href')
href.nodeValue = 'http://www.atlassian.com'
element.setAttributeNode(href);
```


--
# Element
## Selectors

```javascript
Element.getElementById()
Element.getElementsByClassName()
Element.getElementsByTagName()
Element.querySelector()
Element.querySelectorAll()
```

Same than document selectors, but restricted to element descendants

--
# Element
## Selectors

Example:
```javascript
// Find all DIVs in the document
document.getElementsByTagName('div')

// Find all DIVs inside the main SECTION
document.querySelector("section.main").getElementsByTagName('div')
```


---
# Node lists

* Used by all attributes/methods that return a list
* Is an array-like object
* It IS ALIVE!
* It is NOT a node

--
# Node lists
## List of nodes

```javascript
// <ul id="list">
//     <li id="item1">Value 1</li>
//     <li id="item2">Value 2</li>
//     <li id="item3">Value 3</li>
// </ul>

var nodeList = document.getElementsByTagName("li")
nodeList.children.length  //3
nodeList.children[0].id   //"item1"

nodeList instanceof Array //false
```

--
# Node lists
## Is alive

```javascript
// <div id="div1"></div>
// <div id="div2"></div>
// <div id="div3"></div>

var divs = document.getElementsByTagName("div")
divs.length  //3
divs[0].appendChild( document.createElement("div") )
divs.length  //4
```

--
# Node lists
## It is not a node

You can't add a nodelist to another element

```javascript
var nodeList = source.childNodes
target.appendChild(nodeList)  // Error
```

```javascript
// The correct way: iteration
while(node = nodeList[0]) {
    target.appendChild(node)
}
```

The `while` loop works because we are *moving* the nodes somewhere else. As the list is alive, the nodes are removed from the list


---
# Document Fragment

* Lightweight document
* No methods/attributes

--
# Document Fragment
## Like a collection of nodes

```javascript
// <ul id="list">
//     <li id="item1">Value 1</li>
//     <li id="item2">Value 2</li>
//     <li id="item3">Value 3</li>
// </ul>

// Move all LIs to a documentFragment
var nodeList = document.getElementsByTagName("li")
var fragment = document.createDocumentFragment()
while (node = nodeList[0]) fragment.appendChild(node)

// Later
// <ul id="target"></ul>
document.getElementById("target").appendChild(fragment)

// All LIs are appended to "target" in one single operation
```

---
# Events

--
# Events
## Capturing vs Bubbling

![](/resources/dom-events.png)

--
# Events
## Adding listeners

```javascript
Element.addEventListener(eventName, handler [,useCapture=false])
```

Example:

```javascript
document.getElementById("myLink").addEventListener(
    "click",
    function() {alert("Link clicked")},
    true
)
```

Inside the handler, `this` is a reference to the element

--
# Events
## Removing listeners

```javascript
Element.removeEventListener(eventName, handler)
```

Handler must reference the same function!

--
# Events
## Removing listeners

Example (BAD):

```javascript
var element = document.getElementById("myLink")

element.addEventListener("click", function(){alert("Hello")})
element.removeEventListener("click", function(){alert("Hello")})

//Event listener is not removed!
```

--
# Events
## Removing listeners

Example (GOOD):

```javascript
var element = document.getElementById("myLink")
var handler = function(){alert("Hello")}

element.addEventListener("click", handler )
element.removeEventListener("click", handler )
```

--
# Events
## Event object

* All handlers will receive an event object as first argument
* Used to control the behaviour of the browser
* Information about the event

--
# Events
## Event object

Some properties:

* `event.target`: the element that originated the event
* `event.type`: type of the event: 'click', 'mousedown', 'mousepress'...
* `event.keyCode`: which key was pressed (for keyboard-related events)
* `event.button`: which button was pressed (for mouse-related events)
* ...

See [Event Interface](https://developer.mozilla.org/en-US/docs/Web/API/Event#DOM_Event_interface)

--
# Events
## Stopping the browser

Tells the browser to *not* do the default behaviour

```javascript
Event.preventDefault()
```

Example:

```javascript
//<a id="myLink" href="http://www.bing.com">Search</a>

var myLink = document.getElementById("myLink");
myLink.addEventListener('click', function(ev) {
    alert("We don't use BING here ಠ_ಠ")
    ev.preventDefault();
});
```

--
# Events
## Stopping other listeners

Stop the event for being propagated in the tree

```javascript
Event.stopPropagation()
```

You should not do this, as it effectively prevents other handlers from being executed.


--
# Events
## Event delegation

Instead adding listeners to the element, handle the event in a parent element

--
# Events
## Event delegation

Example:

```HTML
<div>
    <button id="copy">Copy</button>
    <button id="cut">Cut</button>
    <button id="paste">Paste</button>
</div>
```

```javascript
//Without event delegation

document.getElementById("copy").addEventListener("click", function() {
    Editor.doAction("copy")
}}
document.getElementById("cut").addEventListener("click", function() {
    Editor.doAction("cut")
}}
document.getElementById("paste").addEventListener("click", function() {
    Editor.doAction("paste")
}}
```

--
# Events
## Event delegation

Example:
```HTML
<div id="actions">
    <button action="copy">Copy</button>
    <button action="cut">Cut</button>
    <button action="paste">Paste</button>
</div>
```

```javascript
//With event delegation

document.getElementById("actions").addEventListener(
    "click",
    function(ev) {
        var action = ev.target.getAttribute("action")
        Editor.doAction(action)
    }
)
```

--
# Events
## Event delegation

Benefits:

* Just one handler to add/remove
* Can add new markup without changing JS code


--
# Events
## Event delegation

*Don't abuse it*

```javascript
document.addEventListener("click", function(ev) {
    var el = ev.target;

    if (el.id == "copy") {
        //do something
    }else if (el.id == "copy") {
        //do something
    }else if (el.nodeName == "a") {
        //do something
    }else if (el.className == "admin-only") {
        //do something
    )else if...

})
```

---
# Performance

--
# Performance
## DOM is very slow

* DOM access is the slowest operation in JavaScript.
* The DOM is a (bad) database, every query has a performance cost.

--
# Performance
## Searching

* Search by className or tagName is VERY expensive
* Search by ID is a bit less expensive

*NEVER search for the same element twice*

--
# Performance
## Searching

*Bad* example:

```javascript
var id = document.getElementById("myButton").id
var class = document.getElementById("myButton").className
```

*Good* example:

```javascript
var element = document.getElementById("myButton")
var id = element.id
var class = element.className
```

--
# Performance
## NodeList are expensive

Every access to a nodelist runs the query again!

--
# Performance
## NodeList are expensive

*Bad* example:

```javascript
var divs = document.getElementsByTagName("div");

for (var i=0; i<divs.length; i++) {
    // Every iteration, the NodeList is re-run to calculate the new length
}
```

*Good* example:

```javascript
var divsNodeList = document.getElementsByTagName("div");

//Convert to array
var divs = Array.prototype.slice.call(divNodeList, 0);

for (var i=0; i<divs.length; i++) {
    // Ok
}
```

--
# Performance
## Properties

Every time you access a DOM property, the property is resolved in realtime

--
# Performance
## Properties

*Bad* example:

```javascript
var element = document.getElementById("myButton")

if (element.className == "admin") {
    //Do something
}else if (element.className == "user" ) {
    //Do something else
}
```

*Good* example:

```javascript
var element = document.getElementById("myButton")
var className = element.className;

if (className == "admin") {
    //Do something
}else if (className == "user") {
    //Do something else
}
```

--
# Performance
## Reflows

When the page layout change, the browser needs to stop everything and redraw the UI. Caused by:

* Add/remove nodes
* Modify element dimensions (using JS or CSS)
* Read element dimensions

--
# Performance
## Reflows

*Bad* example

```javascript
var myDiv = document.createElement("div");

document.body.appendChild(div);              //Reflow
div.className="alert";                       //Potential reflow
div.style.width="10px";                      //Another reflow

var text=document.createTextNode("Warning!");
div.appendChild( text );                     //Are you kidding me?? ಠ_ಠ
```

*Good* example

```javascript
var myDiv = document.createElement("div");

//No reflow, as myDiv is not in the document yet
div.className="alert"
div.style.width="10px";
div.appendChild(document.createTextNode("Warning!")

//Just one reflow
document.body.appendChild(div)
```

--
# Performance
## Memory leaks

Usually created when two objects has a reference to each other and can not be garbage-collected

--
# Performance
## Memory leaks

*Bad* example

```javascript
function() {
    var element = document.getElementById("myDiv");
    element.addEventListener("click", function() {
        //do something
    });
}

//The function has a reference to the element (via closure)
//The element has a reference to the function (via event)

//Nothing can be GarbageCollected!!

```

--
# Performance
## Memory leaks

*Good* example

```javascript
function() {
    var element = document.getElementById("myDiv");
    element.addEventListener("click", function() {
        //do something
    });

    // Break the closure: the function does not have access
    // to the element anymore.
    element = null;
}

```

---
# QUESTIONS?

---
# CODING TIME!

![](/resources/pairing-time.png)

```bash
$ grunt practice:dom
```