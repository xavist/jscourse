# JavaScript Course
## Types

<!-- .slide: data-slide-type="header" -->

---
# Native types

--
# Native types
## Identifying types

![](/resources/types.svg)

--
# Native types
## Identifying types

Use `typeof` to identify a type

```javascript
typeof "test"   //"string"

var age = 42
typeof age      //"number"

typeof {}       //"object"
```

--
# Native types
## Identifying types

Values of `typeof`
![](/resources/types-with-typeof.svg)

--
# Native types
## Identifying types

Use `instanceof` to identify an object

```javascript
var obj = {name: "charlie"}
obj instanceof Object   //true

var arr = [1,2,3];
arr instanceof Object  //true
arr instanceof Array   //true

function test(){}
test instanceof Object   //true
test instanceof Function //true
```

---
# string

![](/resources/types-string.svg)

--
# string

* UTF8 support
* Use double or single quotes
* Use `+` to concatenate

```javascript
console.log("hello" + ' ◕‿◕' ) //"hello ◕‿◕"
```

---
# boolean

![](/resources/types-boolean.svg)

--
# boolean
## False values

* `false`
* `null`
* `undefined`
* `NaN`
* `0`
* `""`

--
# boolean
## True values

* `true`
* Any number other than `0`
* Any non-empty string
* All objects:
  * Empty object `{}` is true!
  * Empty array `[]` is true!
  * `new Boolean(false)` is true!

--
# boolean
## AND operator `&&`

```javascript
3 && 0 && 1   //0     - first false value
"" && 0       //""    - first false value

1 && 2 && 3   //3     - last value
"test" && 42  //42    - last value
true && false //false - last value
```
Returns the *first* false value, or the *last* value

```javascript
if (debug) {
  console.log("Debug mode ON")
}
//Equivalent to
debug && console.log("Debug mode ON")
```
Equivalent to `if`

--
# boolean
## OR operator `||`

```javascript
"test" || 42             //"test" - first true value
0 || 2 || 3              //2      - first true value
false || true            //true   - first true value

0 || 42                  //42    - last value
null || undefined || ""  //""    - last value

```
Returns the *first* true value, or the *last* value

```javascript
function print(name) {
  name = name || "charlie"
  console.log(name)
}
print()       //"charlie"
print("test") //"test"
```
Used to provide default values

---
# number

![](/resources/types-number.svg)

--
# number

* Floats and integers are the same thing!
* IEEE 754 standard, like Java
* 64 bits floating point, 53 bits for integers
* Integer range: ± 9007199254740992

--
# number
## Special numbers: Infinity

* `Infinity`
* `-Infinity`

```javascript
console.log( 10 / 0 ) //Infinity
console.log(-10 / 0 ) //-Infinity
```

--
# number
## Special numbers: `NaN`

Special number, means Not-A-Number

```javascript
console.log( 3 * "string" ) //NaN
```

--
# number
## Special numbers: `NaN`

By definition, nothing is equal to `NaN`, not even itself!
```javascript
var a = 3 * "string"
a === NaN  //false
```

How to check for `NaN`
```javascript
a === a                           //False if a is NaN
typeof a == "number" && isNaN(a)  //True if a is NaN
```


--
# number
## Parse string to number

`parseInt()` / `parseFloat()`

```javascript
parseInt("432")      //432
parseInt("3.2")      //3
parseInt("4ever")    //4
parseInt("four")     //NaN
parseInt("3e10")     //3
parseInt("Infinity") //NaN
```

--
# number
## Parse string to number

Works in other bases

```javascript
parseInt("12",  10)  //12
parseInt("FF",  16)  //255
parseInt("0101", 2)  //5
```

* If the string starts with "0x" or "0X", the default base is 16

--
# number
## Convert string to number

`Number()`

```javascript
Number("432")      //432
Number("3.2")      //3.2
Number("4ever")    //NaN
Number("3e10")     //30000000000
Number("Infinity") //Infinity
```

`+`

```javascript
+"432"      //432
+"3.2"      //3.2
+"4ever"    //NaN
+"3e10"     //30000000000
+"Infinity" //Infinity
```

--
# number
## Weirdness with numbers

Be careful with float comparisons
```javascript
0.1 + 0.2 === 0.3      // false
console.log(0.1 + 0.2) // 0.30000000000000004
```

No buffer overflows
```javascript
var max = 9007199254740992
max == max + 1  //true
```


Be careful with big numbers
```javascript
11111111111111111111 * 2  === 22222222222222220000

```


---
# undefined

![](/resources/types-undefined.svg)

--
# undefined

> "Value used when a variable has not been assigned a value"

--
# undefined
##  Check for undefined

```javascript
if (variable === undefined) {}

typeof variable === "undefined"
```

---
# null

![](/resources/types-null.svg)

--
# null

> "value that represents the intentional absence of any object value"

--
# null
##  Check for null

```javascript
if (variable === null) {}
```


---
# CODING TIME!

![](/resources/pairing-time.png)

```bash
$ grunt practice:types:typeof
```

---
# Objects

![](/resources/types-objects.svg)

--
# Objects

* Collection of key:value pairs
* Has a prototype

--
# Objects
## Creating objects

```javascript
//Using object constructor (slow)
var person = new Object()

//Using object literal (use this)
var person = {}
```

--
# Objects
## Adding properties

```javascript
// During object creation
var person = { name: "Charlie" }

// After creating the object
person.age = 42

console.log(person) // { name: "Charlie", age: 42 }
```

--
# Objects
## Removing properties

```javascript
var person = { name: "Charlie", age: 42, gender: "male" }

delete person.name

console.log(person) //{ age: 42, gender: "male" }
```

--
# Objects
## Access to properties using `.`

```javascript
var person = { name: "Charlie" }

console.log(person.name) //"Charlie"
```

--
# Objects
## Access to properties using `[]`

```javascript
var person = { name: "Charlie" }

console.log(person["name"]) //"Charlie"

var key = "name"
console.log(person[key]) //"Charlie"
```

--
# Objects
## Keys are strings

```javascript
var person = { }
person["name"] = "Charlie"
person["year this person was born"] = 2013 //Can contain spaces
person["ಠ_ಠ"] = true                       //Can contain any UTF8 character
person[""] = "nothing"                     //Even an empty string!
```

--
# Objects
## Keys are strings

```javascript
var person = {}
person[3] = "three"
person[true] = "bool"
person[function(){}] = "WTF!!!"

console.log(person)  // { "3": "three",
                     //   "true": "bool",
                     //   "function (){}": "WTF!!!" }

console.log( person[3] )   //three
console.log( person["3"] ) //three
```

--
# Objects
## Default value is `undefined`

```javascript
var person = {}

console.log(person.age) //undefined
```

--
# Objects
## List keys: `Object.keys(obj)`

```javascript
var person = {age: 42, name: "Charlie"}

console.log(Object.keys(person)); //["age", "name"]
```

The order *IS NOT* guaranteed

--
# Objects
## Check for keys: `in`
```javascript
var person = { name: undefined }

console.log("age" in person)  //false
console.log("name" in person) //true
```

--
# Objects
## Iterate with `for..in`

```javascript
var person = { name: "charlie", age: 42, gender: "male" }

for (var key in person) {
  console.log("Key: "+key+ ", Value: "+person[key])
}

//Key: name, Value: charlie
//Key: age, Value: 42
//Key: gender, Value: male
```

The order *IS NOT* guaranteed

--
# Objects
## Can contain any value

```javascript
var test = {
    a:  "string",
    b:  3.14,
    c:  true,
    d:  [1,2,3],
    e:  function(){ console.log(this.a) },  // Look, a method!
    f:  null,
    g:  { a: 1, b: 2, c: 3}
}
```

--
# Objects
## Circular references are OK

```javascript
var test = { }
test.myself = test

console.log(test)
//nodejs:  { myself: [Circular] }
//
//chrome:  ▼ Object {myself: Object}
//           ▼ myself: Object {
//              ▶ myself: Object
//                 ...

JSON.stringify(test) //TypeError: Converting circular structure to JSON
```

--
# Objects
## Defining properties: `Object.defineProperty()`

`Object.defineProperty(obj, name, properties)`

```javascript
var obj = {};
Object.defineProperty(obj, 'name', {})

// Does the property exist?
console.log ('name' in obj); //true
```

--
# Objects
## Defining properties: `value`

```javascript
var obj = {};
Object.defineProperty(obj, 'name', {value: 'atlassian'}); 

// Get the value
console.log(obj.name); //"atlassian"
```

--
# Objects
## Defining properties: `writable`

* Can change the property value

```javascript
var obj = {};
Object.defineProperty(obj, 'name', { writable: false, value: 'atlassian'}); 
Object.defineProperty(obj, 'age', { writable: true, value: 1});

// Change writable property -> ok
obj.age = 42;
console.log(obj.age); //42

// Change read-only property -> NO ERROR, ignored
obj.name = "charlie";
console.log(obj.name); //"atlassian"
```

--
# Objects
## Defining properties: `configurable`

* Can delete the property
* Can change the property configuration

```javascript
var obj = {};
Object.defineProperty(obj, 'name', { configurable: true, writable: false}); 

// Re-configure the property -> ok
Object.defineProperty(obj, 'name', { writable: true} ); 

// Delete the property -> ok
delete obj.name;
console.log('name' in obj) //false
```

--
# Objects
## Defining properties: `configurable`

* Can delete the property
* Can change the property configuration

```javascript
var obj = {};
Object.defineProperty(obj, 'name', { configurable: false}); 

// Re-configure the property -> ERROR
Object.defineProperty(obj, 'name', { writable: true} );

// Delete the property -> ERROR
delete obj.name;
console.log('name' in obj) //true
```


--
# Objects
## Defining properties: `enumerable`

* Listed in `for..in` loops
* Listed in `Object.keys()`

```javascript
var obj = {};
Object.defineProperty(obj, 'name', { enumerable: false}); 

// Using Object.keys
Object.keys(obj, 'name') //[]

// Using `for..in` loop
for(key in obj) { console.log(key); }  //Nothing

// Using the `in` operator -> always works
console.log('name' in obj) //true
```


--
# Objects
## Defining properties: defaults

```javascript
var obj = {};

Object.defineProperty(obj, 'name', { });
// equivalent to:
Object.defineProperty(obj, 'name', {
  configurable: false,
  enumerable: false,
  value: undefined,
  writable: false
});

obj.age = 3;
// equivalent to:
Object.defineProperty(obj, 'name', {
  configurable: true,
  enumerable: true,
  value: 3,
  writable: true
});
```

--
# Objects
## Defining properties: Getters

```javascript
var obj = {};

Object.defineProperty(obj, 'name', { 
  get: function() {
    return "this is the name";
  }
}); 
console.log(obj.name); //"this is the name"
```

--
# Objects
## Defining properties: Setters

```javascript
var obj = {};

//NOT WORKING
Object.defineProperty(obj, 'name', {
  set: function(v) {
    this.name = v.toUpperCase();
  }
}); 
 
obj.name = "charlie"; //Error: Maximum call stack size exceeded
```

--
# Objects
## Defining properties: Setters

```javascript
var obj = {};

Object.defineProperty(obj, 'name', { configurable: true,
  set: function(v) {
    Object.defineProperty(obj, 'name', {value: v.toUpperCase()});
  }
}); 
 
obj.name = "charlie";
console.log(obj.name) //"CHARLIE" 
```

--
# Objects
## Protecting objects: Prevent extensions

* Can't *add* new properties

```javascript
var obj = {};

Object.preventExtensions(obj);

// Adding new property -> NO ERROR, ignored
obj.name = "test";
console.log(obj); //{}
```

But you can `delete` them

--
# Objects
## Protecting objects: Sealing

* Can't add new properties
* Can't delete
* Can't configure properties

```javascript
var obj = {age: 3};

Object.seal(obj);
```

Equivalent to make all properties `{configurable: false}`

--
# Objects
## Protecting objects: Freezing

* Can't add new properties
* Can't delete
* Can't configure properties
* Can't change values

```javascript
var obj = {age: 3};

Object.freeze(obj);
```

Equivalent to make all properties `{configurable: false, writable: false}`


---
# String/Number/Boolean

![](/resources/types-string-number-boolean.svg)

--
# String/Number/Boolean
## Used as function: `String()`

Converts any value to string

```javascript
typeof String(X) == "string"   //true for all X values
```

Internally, it calls `X.toString()`

Same for `Number()` / `Boolean()`

--
# String/Number/Boolean
## Used as constructor: `new String()`

Creates a `String` object that wraps a string value

```javascript
typeof new String(X) == "object"  //true for all X values
```

```javascript
var nameObject = new String("myName")

//Use String methods
nameObject.contains(nameObject.charAt(1)) //true
```

Same for `new Number()` / `new Boolean()`

--
# String/Number/Boolean
## Autoboxing

We can use `String` methods on regular strings

```javascript
"test".contains("est") //true

//Internally transformed to
(new String("test")).contains("est") //true
```

The wrapper is destroyed after evaluating the expression

```javascript
var a = "test"
a.name = "myName"
console.log(a.name) //undefined, what?

//Internally transformed to

var a = "test"
(new String(a).name = "myName"
console.log((new String(a)).name)  //This wrapper is a different instance
```

---
# Array

![](/resources/types-array.svg)

--
# Array
## Creation

Using constructor

```javascript
var a = new Array(3) //Creates an array with 3 undefined elements
```

Using literal (use this)

```javascript
var a = []       //Creates an empty array
var a2 = [1,2,3] //Creates an array with 3 elements
```

--
# Array
## Magic property: `length`

![](/resources/magic_meme.gif)

("magic" -> uses getters and setters)

--
# Array
## Magic property: `length`

Magic: `length` is a property, but its value is computed in real-time

```javascript
// Empty array
var a = []
console.log(a, a.length)  //[], 0

// Can have gaps
a[1] = "test1"
console.log(a, a.length)  //[undefined, "test1"], 2

// Use string as indexes
a["2"] = "test2"
console.log(a, a.length)  //[undefined, "test1", "test2"], 3

// Store any property
a.myProperty = "test3"
console.log(a, a.length)  //[undefined, "test1", "test2"], 3
console.log(a.myProperty) //"test3"
```
Returns the biggest integer key + 1 (or 0 if no key)



--
# Array
## Magic property: `length`

Magic: `length` is a property, but setting its value has side effects

```javascript
var a = ["a","b","c","d"]
console.log(a.length) //4

a.length = 2
console.log(a) //["a","b"]

a.length = 5
console.log(a) //["a","b", undefined, undefined, undefined]
```
Used to set the size of an array



--
# Array
## `Array` methods

| Mutate  | Creation | String conversion  |
|---      |---       | ---                |
|pop      | concat   | toString           |
|push     | slice    | join               |
|reverse  |          |                    |
|shift    |          |                    |
|sort     |          |                    |
|splice   |          |                    |
|unshift  |          |  &nbsp;            |

See [Array Methods](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/prototype#Methods)

WARNING: some methods *create* a new array, some methods *modify* the existing array

--
# Array
## Iteration: `forEach()`

```javascript
var a = [1, 2, 3, 4];

a.forEach(function(item) {
  console.log(item * item);
})
//1, 4, 9, 16
```


--
# Array
## Iteration: `every()`

```javascript
var a = [1, 2, 3, 4];

var lessThanFive = a.every(function(item) {
  return item < 5;
});

console.log(lessThanFive) //true
```

Retruns `true` if `callback` returns true for all elements.


--
# Array
## Iteration: `some()`

```javascript
var a = [1, 2, 3, 4];

var lessThanTwo = a.some(function(item) {
  return item < 2;
});

console.log(lessThanTwo) //true
```

Retruns `true` if `callback` returns true for at least one element.


--
# Array
## Iteration: `map()`

```javascript
var a = [1, 2, 3, 4];

var plusOne = a.map(function(item) {
  return item + 1;
});

console.log(plusOne) //[2, 3, 4, 5]
```

Applies `callback` to each item, and creates a new array with the results.

--
# Array
## Iteration: `filter()`

```javascript
var a = [1, 2, 3, 4];

var lessThanThree = a.filter(function(item) {
  return item < 3;
});

console.log(lessThanThree) //[1, 2]
```

Applies `callback` to each item, and creates a new array with the items that returns `true`

--
# Array
## Iteration: `reduce()`

```javascript
var a = [1, 2, 3, 4];

var sum = a.reduce(function(previousValue, item) {
  return previousValue + item;
});

console.log(sum) //10
```

Applies `callback` to each item, and creates a value as result


---
# Error

![](/resources/types-error.svg)

--
# Error

Parent class for all JavaScript errors. Error types:

* `EvalError`: error with `eval()`
* `RangeError`:  internal to the JavaScript engine (eg. "InternalError: too much recursion")
* `ReferenceError`: when  a non-existent variable is referenced
* `SyntaxError`: when interpreting syntactically invalid code
* `TypeError`: when a value is not of the expected type
* `URIError`: global URI handling function was used in a wrong way

--
# Error
## Throwing errors

```javascript
if (user === false) {
    throw new Error("User is false, something is broken!")
}
```

Actually, you can throw any value.

```javascript
if (user === false) {
    throw {msg: "Boken", severity: "A lot"}
}
```


---
# Comparing Types

--
# Comparing Types
## `===` and `!==`

```javascript
a === b
```

* For scalars: Check that values *and types* are the same
* For objects: Check that both variables points to the same instance

--
# Comparing Types
## `==` and `!=`

```javascript
a == b
```

Check that values are the same. Cast them to the same type.

--
# Comparing Types
## `==` and `!=`

Rules:

1) If they are the same type, use `===`

```javascript
3 == 3   // is equivalent to 3 === 3
```

--
# Comparing Types
## `==` and `!=`

Rules:

2) `undefined` and `null` equal each other and nothing else

```javascript
undefined == null   // true
undefined == false  // false
null == 0           // false
```

--
# Comparing Types
## `==` and `!=`


Rules:

3) `<number> == true` → true if `<number> === 1`

```javascript
3 == true   // false
1 == true   // true
```

--
# Comparing Types
## `==` and `!=`


Rules:

4) `<string> == true` → true if `<string> === "1"`

```javascript
"1" == true     // true
"true" == true  // false
```

--
# Comparing Types
## `==` and `!=`


Rules:

5) `<string> == <number>` → string is converted to number

```javascript
"1" == 1        // true
"-45" == -45    // true
```

--
# Comparing Types
## `==` and `!=`


Rules:

6) If `<object> == <string>` → call `object.toString()`

```javascript
var obj = { toString: function() {return "test";} }
obj == "test"  // true
```

--
# Comparing Types
## `==` and `!=`


Rules:

7) If `<object> == <non-string>` → call `object.valueOf()`

```javascript
var obj = { valueOf: function() {return 42;} }
obj == 42  // true
```

--
# Comparing Types
## `==` and `!=`

![](/resources/types-matrix.png)

--
# Comparing Types
## Conditionals

This
```javascript
if (x) {}
```

is *not* the same than

```javascript
if (x == true) {}
```

and is *not* the same than

```javascript
if (x === true) {}
```

--
# Comparing Types
## Conditionals

Rules:

1. `undefined` and `null` are false
2. `<number>` is false for the values `0` and `NaN`
3. `<string>` is false for `""`
4. All objects are true

--
# Comparing Types
## Conditionals

Examples:

```javascript
// These are true
if ("2") {}
if (new Boolean(false)) {}

// These are false
if ("2" == true) {}
if (new Boolean(false) == true) {}
```

--
# Comparing Types
## Weirdness

All examples are true

```javascript
"  \t\n\r" == 0   // Because the definition of empty string
```
```javascript
1 == [[1]]        // Beacause type casts

//  [[1]] -> ["1"] Internal array is converted to string
//  ["1"] ->  "1"  Converted to string again
//   "1"  ->   1   And converted into a number
```

```javascript
'[object Object]' == {}
// Because `{}.toString()` returns '[object Object]' by default
```

---

# QUESTIONS?

---

# CODING TIME!

![](/resources/pairing-time.png)

```bash
$ grunt practice:types:schema
```
