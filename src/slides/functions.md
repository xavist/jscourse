# JavaScript Course
## Functions

<!-- .slide: data-slide-type="header" -->

---
# First-class citizens

--
# First-class citizens
## Function Declarations

Can appear anywhere in the code
```javascript
function hello(){
    console.log("Hello, world")
}
hello()
```

--
# First-class citizens
## Function Expression

Can appear anywhere in the code too
```javascript
var hello = function (){
    console.log("Hello, world")
}
hello()
```


--
# First-class citizens
## Function Expression

Can appear inside other expressions
```javascript
var talk = (sayHello==true) ?
    (function (){console.log("Hello, world")}):
    (function (){console.log("Goodbye, world")})
talk()
```


--
# First-class citizens
## Function Expression

Can be passed as argument
```javascript
function run(func) {
    func()
}

run(function(){
    console.log("Hello, world")
})
```


---
# Creating a function

--
# Creating a function
## Using a Literal

```javascript
function [name] ( [arg1[, arg2[, ... argN]]] ) { functionBody }
```

Example:
```javascript
function hello(name){
    console.log('Hello, '+name)
}
```

--
# Creating a function
## Using Function Constructor

```javascript
// All arguments are strings
new Function ( [arg1[, arg2[, ... argN]],] functionBody )
```

Example:
```javascript
var hello = new Function ( "name", "console.log('Hello, '+name)" )
```

---
# IIFE: Immediately-invoked function expressions

--
# IIFE: Immediately-invoked function expressions

Define a function and run it immediately.
```javascript
//Two similar forms
(function(){ /* code */ }(/* arguments */))
(function(){ /* code */ })(/* arguments */)
```

--
# IIFE: Immediately-invoked function expressions
It is equivalent to define a temporary function and call it right away

```javascript
var secret = (function(){
  return 42
})()
```

is similar to

```javascript
function temp(){
  return 42
}
var secret = temp()
```

--
# IIFE: Immediately-invoked function expressions
## Prevent globals

```javascript
var PRIVATE_KEY = "abc123"
function AES(msg, key) { /*...*/}
function encrypt(msg) {
    return AES(msg, key)
}
// AES and PRIVATE_KEY are global, anyone can read/modify them.
```

With IIFE
```javascript
var encrypt = (function(){
    //Private
    var PRIVATE_KEY = "abc123"
    function AES(msg, key) { /*...*/}

    //Public
    return function(msg) {
        return AES(msg, key)
    }
})()
// AES and PRIVATE_KEY are not global, only encrypt() is exposed
```

---
# Function arguments

--
# Function arguments
## Arguments are optional

Functions can be called with any number of arguments
```javascript
var test = function(a, b) {
    console.log(a, b)
}

test(1,2)     // 1, 2
test()        // undefined, undefined
test(1,2,3,4) // 1, 2
```

--
# Function arguments
## Passed by reference

```javascript
function makeDoctor( person ) {
    person.name = "Dr. " + person.name
}

var doom = { name: "Doom" }
makeDoctor(doom)

console.log(doom.name)  //"Dr. Doom"
```

--
# Function arguments
##...unless you use =

```javascript
function makeDoctor( person ) {
    person = { name: "Dr. Doom" }
}

var doom = { name: "Doom" }
makeDoctor(doom)

console.log(doom.name)  //"Doom"
```

--
# Function arguments
## `arguments` object

Object with all arguments, indexed by position
```javascript
function hello() {
    console.log(arguments[0])
}
hello("world")   //"world"
```

--
# Function arguments
## `arguments.length`

Is the number of arguments actually passed to the function
```javascript
function sum() {
  console.log(arguments.length) //3
}
sum(1,2,3)
```

--
# Function arguments
## Useful for variadic functions

Loop over arguments
```javascript
function sum( ) {
    var total = 0
    for (var i = 0; i < arguments.length; i++) {
        total = total + arguments[i]
    }
    return total
}
sum(1,2,3) // 6
```

--
# Function arguments
## `arguments` is not an array

```javascript
function test( ) {
    console.log( arguments.sort() )
}
test(1,2,3) //TypeError: Object #<Object> has no method 'sort'
```

--
# Function arguments
## Can be converted to array

```javascript
function test( ) {

    var args = [];
    for (var i = 0; i < arguments.length; i++) {
      args[i] = arguments[i];
    }
    
    console.log( args.sort() )
}
test(3,2,1) //[1, 2, 3]
```


---
# Functions as objects

--
# Functions as objects
## A function is an object

It can contain properties
```javascript
function hero() {
    return "I'm Batman"
}
hero()  //"I'm Batman"

hero.superpower = "Lots of money"
console.log(hero.superpower) // "Lots of money"
```

--
# Functions as objects
## Predefined property: `name`

The name of the function
```javascript
function hero(a, b, c) {}

console.log(hero.name) //"hero"
```

--
# Functions as objects
## Predefined property: `length`

The number of arguments *declared* in the function
```javascript
function hero(a, b, c) {}

console.log(hero.length) //3
```

(not the same than `arguments.length`)

--
# Functions as objects
## Predefined method: `toString`

Gets the function declaration as a `string`
```javascript
function sum(a,b) { return a+b }

console.log(sum.toString()) //"function sum(a,b) { return a+b }"
```

* Very useful for debugging
* Angular uses this to do Dependency Injection

--
# Functions as objects
## Method: `call`/`apply`

Used to pass a custom value as `this`

```javascript
function getName() {
    return this.name
}

getName.apply( {name: "test"} )
getName.call( {name: "test"} )
```

--
# Functions as objects
## Method: `call`/`apply`

Used to pass a variable number of arguments

```javascript
function sum () {
    var total = 0
    for (var i = 0; i < arguments.length; i++) {
        total = total + arguments[i]
    }
    return total
}

sum.call ( {},   1, 2, 3, 4   ) // 10
sum.apply( {}, [ 1, 2, 3, 4 ] ) // 10

//sum() is not using `this`, so the first argument does not matter
```

* `call` accepts a list of individual arguments
* `apply` accepts a single array of arguments.



---
# `this` inside functions

--

# `this` inside functions

* `this` value is determined at runtime
* It depends on how the function is invoked.
* There are (at least) 5 ways to specify the `this` value

--
# `this` inside functions
## 1. Naked functions

```javascript
func()
```

`this` is the global object (`window` in browsers)

```javascript
function myFunction() {
    return this.document
}
myFunction() === window.document //true
```

--
# `this` inside functions
## 2. Object functions (aka methods)

```javascript
obj.func()
```

`this` is the object before the dot (`obj` in this case)

```javascript
var person = {
    name: "Charlie",
    greet: function(){
        return "Hello, I am " + this.name
    }
}
person.greet() //"Hello, I am Charlie"
```

--
# `this` inside functions
## 3. Using `new`

```javascript
new func()
```

`this` is an empty object (returned by default)

```javascript
function Person(name) {
    this.name = name
}

var charlie = new Person("Charlie")
console.log(charlie.name)  //"Charlie"
```

--
# `this` inside functions
## 4. Using `call`/`apply`

```javascript
func.call(obj, ...)
func.apply(obj, ...)
```

`this` is `obj`

```javascript
var person = {name: "Charlie"}
var getName = function() {
    return this.name
}

getName.apply(person) //"Charlie"
```

--
# `this` inside functions
## 5. Using DOM events

```html
<button onclick="doClick"></button>
```

`this` is the `HTMLElement`

```javascript
function doClick() {
    console.log( this.tagName ) //"button"
}
```

--
# `this` inside functions
## Determined when function is invoked

The *same* function can have different values for `this`

```javascript
function getAge() {
    return this.age
}

age = 10
var person = { age: 30, getAge: getAge }

getAge()                //10, equivalent to window.age
getAge.call({age: 20})  //20
person.getAge()         //30, equivalent to person.age
```


--
# `this` inside functions
## Lock `this` value

```javascript
function getPower() {
    console.log(this.power)
}
var warrior = {power: 9001}

// Native
var boundGetPower = getPower.bind(warrior)

// Using underscore.js
var boundGetPower = _.bind(getPower, warrior)
```

Now we have two functions:

* `getPower`: the value of `this` can change, as seen before
* `boundGetPower`: the value of `this` is *always* `warrior`

---

# Closures

--
# Closures
## Definition

Closure: a function that holds a reference to variables from outside its own scope

--
# Closures
## Access to parent variables
A function can access all the variables from its "enclosing" (or parent) function

```javascript
// Parent is a function that returns another function
function parent(name) {
    var age = 10

    return function() {
        // "name" and "age" are not local variables
        return name + ": " + age
    }
}

var closure = parent("Charlie")
closure() // "Charlie: 10"
```

--
# Closures
## Multi-level access


```javascript
var buildSum = function () {
    var A = 1
    return function () {
        var B = 2
        return function (C) {
            return A + B + C
        }
    }
}

buildSum()()() //6 (1+2+3)
```

--
# Closures
## Shared access

```javascript
var buildGetSet = function (initialValue) {
    var value = initialValue
    return {
        get: function() {
            return value
        },
        set: function(newValue) {
            value = newValue
        }
    }
}

var age = buildGetSet(10)
age.get() //10
age.set(20)
age.get() //20
age.value //undefined
```

---

# Scope

--
# Scope

JavaScript has function-level scope, no block scope.


--
# Scope
## Function scope

```javascript
var a = 1;
function test () {
  var a = 2;
  console.log(a)
}

test();         //2;
console.log(a); //1;
```

--
# Scope
## `var` keyword

Creates a new variable in the current scope

Without `var`:

```javascript
var name = "test";
function test () {
  name = 2
  console.log(name)
}

test();            //2
console.log(name); //2 ????
```

--
# Scope
## `var` keyword

Creates a new variable in the current scope

With `var`:

```javascript
var name = "test";
function test () {
  var name = 2
  console.log(name)
}

test();            //2
console.log(name); //"test"
```

--
# Scope
## Shadowing variables

Variables/arguments defined inside the function hide external variables
```javascript
var a = 1;
var b = 2;

function test( b ) {
  var a = 10;
  console.log( a + b );
}
test(20) //30
```

`this` and `arguments` are always shaded

--
# Scope
## Hoisting

`var` expressions and function *declarations* are moved to the top of body before executing the function

```javascript
function sayHello( ) {
  speak();
  function speak() {
    console.log("Hello")
  }
}
sayHello(); //"Hello"
```

equivalent to

```javascript
function sum( ) {
  function speak() {
    console.log("Hello")
  }
  speak();
}
```

--
# Scope
## Hoisting

`var` expressions and function *declarations* are moved to the top of body before executing the function

```javascript
function test( ) {
  console.log(a);
  var a = 3;
}
test(); //undefined
```

equivalent to

```javascript
function test( ) {
  var a;
  console.log(a);
  a = 3;
}
```

--
# Scope
## Hoisting

`var` expressions and function *declarations* are moved to the top of body before executing the function

```javascript
function sayHello( ) {
  speak();
  var speak = function doSum() {
    console.log("Hello")
  }
}
sayHello(); //ERROR!!
```

equivalent to

```javascript
function sayHello( ) {
  var speak;
  speak();
  speak = function doSum() {
    console.log("Hello")
  }
}
```


---
# Execution context

--
# Execution context

* Object internal to the JavaScript engine

* Associated with a function

* Contains all the references (variables/functions) the function can access.

* Created in two phases
  * On function creation
  * On function execution

--
# Execution context
## Example function

```javascript
function foo() {
    function bar(a, b){
        function test(){ alert("test!"); }
        var name = "myName";
    }
}
```

Execution context
```javascript
bar_execution_context = {
}
```

--
# Execution context
## Function creation, Step 1

```javascript
function foo() {
    function bar(a, b){
        function test(){ alert("test!"); }
        var name = "myName";
    }
}
```

`arguments` object is created

```javascript
bar_execution_context = {
  arguments: {}
}
```

--
# Execution context
## Function creation, Step 2

```javascript
function foo() {
    function bar(a, b){
        function test(){ alert("test!"); }
        var name = "myName";
    }
}
```

function arguments are created with `undefined` value

```javascript
bar_execution_context = {
  arguments: {},
  a: undefined,
  b: undefined
}
```

--
# Execution context
## Function creation, Step 3

```javascript
function foo() {
    function bar(a, b){
        function test(){ alert("test!"); }
        var name = "myName";
    }
}
```

Function body is scanned to extract `var` declarations

```javascript
bar_execution_context = {
  arguments: {},
  a: undefined,
  b: undefined,
  name: undefined
}
```

--
# Execution context
## Function creation, Step 4

```javascript
function foo() {
    function bar(a, b){
        function test(){ alert("test!"); }
        var name = "myName";
    }
}
```

Function body is scanned to extract function declarations

```javascript
bar_execution_context = {
  arguments: {},
  a: undefined,
  b: undefined,
  name: undefined,
  test: function()
}
```

--
# Execution context
## Function creation, Step 5

```javascript
function foo() {
    function bar(a, b){
        function test(){ alert("test!"); }
        var name = "myName";
    }
}
```

A `[scope]` attribute is created, it points to parent ExecutionContext

```javascript
bar_execution_context = {
  arguments: {},
  a: undefined,
  b: undefined,
  name: undefined,
  test: function(),
  [scope]: foo_execution_context
}
```

--
# Execution context
## Function execution, Step 1

```javascript
bar("a")
```

`arguments` is populated with `.length` and the actual values

```javascript
bar_execution_context = {
  arguments: {
    length: 1,
    0: "a"
  },
  a: undefined,
  b: undefined,
  name: undefined,
  test: function(),
  [scope]: foo_execution_context
}
```

--
# Execution context
## Function execution, Step 2

```javascript
bar("a")
```

arguments are initialized with the actual values (if present)

```javascript
bar_execution_context = {
  arguments: {
    length: 1,
    0: "a"
  },
  a: "a",
  b: undefined,
  name: undefined,
  test: function(),
  [scope]: foo_execution_context
}
```

--
# Execution context
## Function execution, Step 3

```javascript
bar("a")
```

`this` is created

```javascript
bar_execution_context = {
  arguments: {
    length: 1,
    0: "a"
  },
  a: "a",
  b: undefined,
  name: undefined,
  test: function(),
  [scope]: foo_execution_context,
  this: window
}
```

--
# Execution context
## FINAL STEP

The engine starts to execute the function's body, reading/writing the values saved in the ExeucutionContext

--
# Execution context
## When reading a reference

```javascript
function bar() {
  //...
  console.log(a); // <------ reading a
}
```

1. If the key is present in the current ExecutionContext, return the value
2. If the key is present in the `[scope]` ExecutionContext, return the value
3. Repeat step 2 until there are no more ExecutionContext in the chain
4. Thrown an `Error`


--
# Execution context
## When writing a reference

```javascript
function bar() {
  //...
  a = 1; // <------ writing a
}
```

1. If the key is present in the current ExecutionContext, write the value
2. If the key is present in the `[scope]` ExecutionContext, write the value
3. Repeat step 2 until there are no more ExecutionContext in the chain
4. Write the value in the global ExecutionContext

--
# Execution context
## Global variables

We are just accessing references from the last ExecutionContext in the chain, the global ExecutionContext

--
# Execution context
## Closures

We are just accessing references from the `[scope]` ExecutionContext


--
# Execution context
## Hoisting

References are already defined in the ExecutionContext before the function body actually starts running

--
# Execution context
## Shadowing

Because we start searching from the bottom ExecutionContext all the way to the top, and we stop when we
find the reference.

Example: you can't access `arguments` from your parent function, because the search algorith will find your
own `arguments` first.

--
# Execution context
## `this` value is variable

...because `this` is created on function invocation, just like all the arguments


---
# Execution context - Examples

--
# Execution context - Examples

Code:
```javascript
var buildGetSet = function (initialValue) {
    var value = initialValue;
    return {
        get: function() {
            return value;
        },
        set: function(newValue) {
            value = newValue
        }
    }
}

var age = buildGetSet(10)
age.get() //10
age.set(20)
age.get() //20
age.value //undefined

```

--
# Execution context - Examples

Execution contexts:
```javascript
// `this` and `arguments` are omitted

EC_buildGetSet = {
    "[scope]": EC_global,
    "initialValue": undefined,
    "value": undefined
}

EC_get = {
    "[scope]": EC_buildGetSet
}

EC_set = {
    "[scope]": EC_buildGetSet,
    "newValue": undefined
}

```

--
# Execution context - Examples
## Count from 1 to 10 seconds

```javascript
for (var i = 1; i < 10; i++) {
    setTimeout(function() {
        console.log(i);
    }, i*1000);
}
```
*WRONG*


--
# Execution context - Examples
## Fixed version

```javascript
function createLoggerForNumber(n) {
    return function() {
        console.log(n);
    }
}

for (var i = 1; i < 10; i++) {
    setTimeout(createLoggerForNumber(i), i*1000)
}
```
*GOOD*

--
# Execution context - Examples
## Short version

```javascript
// Counts from 1 to 10
for (var i = 1; i < 10; i++) {
    setTimeout((function(n){
        return function() {
            console.log(n)
        }
    })(i), i*1000)
}
```
*BETTER*

--
# QUESTIONS?

--
# CODING TIME!
![](/resources/pairing-time.png)

```bash
$ grunt practice:functions
```
