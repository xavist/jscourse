/**
 * Implement a JSON Schema detector.
 *
 * We have a specification language (i.e. the format expected in a JSON object), and we have to implement a function
 * that checks if an object conforms to the specification. Our current language is:
 *
 * []  -> Detects an array of any length with any type inside. Examples:
 *
 * schema: []     json: [1,2,3]     -> OK
 * schema: []     json: []          -> OK
 * schema: []     json: {a:"name"}  -> FAIL (not an array)
 *
 *
 * [ type1, type2...]  -> Detects an array with a fixed length, where every value match the type provided. The types
 * could be: "string", "number" or "boolean"
 *
 * schema: ["string", "string"]  json:["hello"]    -> FAIL (number of elements does not match)
 * schema: ["string", "string"]  json:["hello", 1] -> FAIL (type of 2nd element does not match)
 * schema: ["string", "string"]  json:["hello", "world"] -> OK
 *
 *
 * {} -> Detects an object with any content (including empty object)
 *
 * schema: {}     json: {}               -> OK
 * schema: {}     json: {name:"charlie"} -> OK
 * schema: {}     json: [1,2,3]          -> FAIL (not an object)
 *
 *
 * {key1: type, key2:type...} -> Detects an object with exactly those keys and values
 *
 * schema: {name:"string"}     json: {name:"charlie"}               -> OK
 * schema: {name:"string"}     json: {name:"charlie", locale:"AU"}  -> FAIL (extra key)
 * schema: {name:"string"}     json: {name:3}                       -> FAIL (bad type for name)
 *
 *
 * Nested arrays/objects are supported:
 *
 * schema: [ "number", "string", [], { isValid: "boolean", options:[] }
 * json:   [ 1, "charlie, [10,20,30,40], { isValid: true, options:["a",1,false] }
 *
 *
 * The tests only validates arrays, objects, numbers, strings and booleans. Don´t waste time with other types. Also,
 * you can use the `Types` object from the previous practice.
 */

function JSONSchema(json, schema) {

  function countProperties(obj) {
    var num = 0;
    for (var i in obj) if (obj.hasOwnProperty(i)) {
      num++
    }
    return num;
  }

  function isValidArray(value, schema) {
    if (schema.length == 0) return Types.isArray(value);
    if (schema.length !== json.length) return false;

    var isValid = true;
    for (var i = 0; isValid && i < schema.length; i++) {
      var expectedType = schema[i];
      var currentValue = value[i];
      isValid = JSONSchema(currentValue, expectedType);
    }
    return isValid;
  }

  function isValidObject(value, schema) {
    if (countProperties(schema)==0) return Types.isObject(value);
    if (countProperties(schema)!=countProperties(value)) return false;

    var isValid = true;
    for (var key in schema) {
      var expectedType = schema[key];
      var currentValue = value[key];
      isValid = JSONSchema(currentValue, expectedType);
      if (!isValid) return false;
    }
    return isValid;
  }

  function isValidScalar(value, schema) {
    return Types.getType(value) == schema;
  }

  if (Types.isArray(schema)) {
    return isValidArray(json, schema)
  } else if (Types.isObject(schema)) {
    return isValidObject(json, schema)
  } else {
    return isValidScalar(json, schema);
  }
}