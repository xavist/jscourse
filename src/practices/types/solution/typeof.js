/**
 * Implement an object called "Types" with the following methods
 *
 *   .isString(val): Checks if a value is a string
 *   .isBoolean(val): Checks if a value is a boolean
 *   .isNumber(val): Checks if a value is a number and not NaN
 *   .isNaN(val): Checks if a value is NaN
 *   .isNull(val): Checks if a value is null
 *   .isUndefined(val): Checks if a value is undefined
 *   .isObject(val): Checks if a value is an object (and not an Array, a Function, etc)
 *   .isArray(val): Checks if a value is an Array
 *   .isFunction(val): Checks if a value is a function
 *   .getType(val): Returns the type as a string ("string", "boolean", "number", "NaN", "undefined", "null", "function",
 *     "object", "array")
 *
 *
 * Implements your methods as object properties. Example:
 *
 *   Types = {
 *     isString: function() {
 *       //your amazing implementation here
 *     },
 *     isBoolean: function() {
 *       //your amazing implementation here
 *     }
 *     ...
 *   }
 */

Types = {
  isString: function(val) {
    return typeof val === "string";
  },

  isBoolean: function(val) {
    return typeof val === "boolean";
  },

  isNumber: function(val) {
    return typeof val === "number" && val===val;
  },

  isNaN: function(val) {
    return val !== val;
  },

  isNull: function(val) {
    return val === null;
  },

  isUndefined: function(val) {
    return typeof val === "undefined"
  },

  isObject: function(val) {
    return typeof val === "object"
      && val !== null
      && !(val instanceof Array)
  },

  isArray: function(val) {
    return val instanceof Array
  },

  isFunction: function(val) {
    return typeof val === "function"
  },

  getType: function(val) {
    switch (typeof val) {
      case "string":
      case "boolean":
      case "undefined":
      case "function":
        return typeof val;
      case "number":
        return (val!==val)?"NaN":"number";
      case "object":
        if (val === null) return "null";
        if (val instanceof Array) return "array";
        return "object"
    }
  }
};