var assert = chai.assert;
var expect = chai.expect;
mocha.setup({globals: ['setTimeout', 'setInterval', 'clearTimeout', 'clearInterval']});

describe("Types Practice 1,", function(){

    describe("isString method:", function() {
        it("should detect string values", function(){
            assert.isTrue ((Types.isString("string"))            , "Checking string");
            assert.isTrue ((Types.isString(""))                  , "Checking empty string");
            assert.isFalse((Types.isString(true))                , "Checking true");
            assert.isFalse((Types.isString(false))               , "Checking false");
            assert.isFalse((Types.isString(3.14))                , "Checking decimal");
            assert.isFalse((Types.isString(NaN))                 , "Checking NaN");
            assert.isFalse((Types.isString(null))                , "Checking null");
            assert.isFalse((Types.isString(undefined))           , "Checking undefined");
            assert.isFalse((Types.isString())                    , "Checking missing argument");
            assert.isFalse((Types.isString({}))                  , "Checking empty object");
            assert.isFalse((Types.isString(function(){}))        , "Checking function");
            assert.isFalse((Types.isString([]))                  , "Checking empty array");
        });
    });

    describe("isBoolean method:", function() {
        it("should detect boolean values", function(){
            assert.isFalse((Types.isBoolean("string"))             , "Checking string");
            assert.isFalse((Types.isBoolean(""))                   , "Checking empty string");
            assert.isTrue ((Types.isBoolean(true))                 , "Checking true");
            assert.isTrue ((Types.isBoolean(false))                , "Checking false");
            assert.isFalse((Types.isBoolean(3.14))                 , "Checking decimal");
            assert.isFalse((Types.isBoolean(NaN))                  , "Checking NaN");
            assert.isFalse((Types.isBoolean(null))                 , "Checking null");
            assert.isFalse((Types.isBoolean(undefined))            , "Checking undefined");
            assert.isFalse((Types.isBoolean())                     , "Checking missing argument");
            assert.isFalse((Types.isBoolean({}))                   , "Checking empty object");
            assert.isFalse((Types.isBoolean(function(){}))         , "Checking function");
            assert.isFalse((Types.isBoolean([]))                   , "Checking empty array");
        });
    });

    describe("isNumber method:", function() {
        it("should detect number values", function(){
            assert.isFalse((Types.isNumber("string"))             , "Checking string");
            assert.isFalse((Types.isNumber(""))                   , "Checking empty string");
            assert.isFalse((Types.isNumber(true))                 , "Checking true");
            assert.isFalse((Types.isNumber(false))                , "Checking false");
            assert.isTrue ((Types.isNumber(3.14))                 , "Checking decimal");
            assert.isFalse((Types.isNumber(NaN))                  , "Checking NaN");
            assert.isFalse((Types.isNumber(null))                 , "Checking null");
            assert.isFalse((Types.isNumber(undefined))            , "Checking undefined");
            assert.isFalse((Types.isNumber())                     , "Checking missing argument");
            assert.isFalse((Types.isNumber({}))                   , "Checking empty object");
            assert.isFalse((Types.isNumber(function(){}))         , "Checking function");
            assert.isFalse((Types.isNumber([]))                   , "Checking empty array");
        });
    });

    describe("isNaN method:", function() {
        it("should detect NaN values", function(){
            assert.isFalse((Types.isNaN("string"))             , "Checking string");
            assert.isFalse((Types.isNaN(""))                   , "Checking empty string");
            assert.isFalse((Types.isNaN(true))                 , "Checking true");
            assert.isFalse((Types.isNaN(false))                , "Checking false");
            assert.isFalse((Types.isNaN(3.14))                 , "Checking decimal");
            assert.isTrue ((Types.isNaN(NaN))                  , "Checking NaN");
            assert.isFalse((Types.isNaN(null))                 , "Checking null");
            assert.isFalse((Types.isNaN(undefined))            , "Checking undefined");
            assert.isFalse((Types.isNaN())                     , "Checking missing argument");
            assert.isFalse((Types.isNaN({}))                   , "Checking empty object");
            assert.isFalse((Types.isNaN(function(){}))         , "Checking function");
            assert.isFalse((Types.isNaN([]))                   , "Checking empty array");
        });
    });

    describe("isNull method:", function() {
        it("should detect null values", function(){
            assert.isFalse((Types.isNull("string"))             , "Checking string");
            assert.isFalse((Types.isNull(""))                   , "Checking empty string");
            assert.isFalse((Types.isNull(true))                 , "Checking true");
            assert.isFalse((Types.isNull(false))                , "Checking false");
            assert.isFalse((Types.isNull(3.14))                 , "Checking decimal");
            assert.isFalse((Types.isNull(NaN))                  , "Checking NaN");
            assert.isTrue ((Types.isNull(null))                 , "Checking null");
            assert.isFalse((Types.isNull(undefined))            , "Checking undefined");
            assert.isFalse((Types.isNull())                     , "Checking missing argument");
            assert.isFalse((Types.isNull({}))                   , "Checking empty object");
            assert.isFalse((Types.isNull(function(){}))         , "Checking function");
            assert.isFalse((Types.isNull([]))                   , "Checking empty array");
        });
    });

    describe("isUndefined method:", function() {
        it("should detect undefined values", function(){
            assert.isFalse((Types.isUndefined("string"))             , "Checking string");
            assert.isFalse((Types.isUndefined(""))                   , "Checking empty string");
            assert.isFalse((Types.isUndefined(true))                 , "Checking true");
            assert.isFalse((Types.isUndefined(false))                , "Checking false");
            assert.isFalse((Types.isUndefined(3.14))                 , "Checking decimal");
            assert.isFalse((Types.isUndefined(NaN))                  , "Checking NaN");
            assert.isFalse((Types.isUndefined(null))                 , "Checking null");
            assert.isTrue ((Types.isUndefined(undefined))            , "Checking undefined");
            assert.isTrue ((Types.isUndefined())                     , "Checking missing argument");
            assert.isFalse((Types.isUndefined({}))                   , "Checking empty object");
            assert.isFalse((Types.isUndefined(function(){}))         , "Checking function");
            assert.isFalse((Types.isUndefined([]))                   , "Checking empty array");
        });
    });

    describe("isObject method:", function() {
        it("should detect object values", function(){
            assert.isFalse((Types.isObject("string"))             , "Checking string");
            assert.isFalse((Types.isObject(""))                   , "Checking empty string");
            assert.isFalse((Types.isObject(true))                 , "Checking true");
            assert.isFalse((Types.isObject(false))                , "Checking false");
            assert.isFalse((Types.isObject(3.14))                 , "Checking decimal");
            assert.isFalse((Types.isObject(NaN))                  , "Checking NaN");
            assert.isFalse((Types.isObject(null))                 , "Checking null");
            assert.isFalse((Types.isObject(undefined))            , "Checking undefined");
            assert.isFalse((Types.isObject())                     , "Checking missing argument");
            assert.isTrue ((Types.isObject({}))                   , "Checking empty object");
            assert.isFalse((Types.isObject(function(){}))         , "Checking function");
            assert.isFalse((Types.isObject([]))                   , "Checking empty array");
        });
    });

    describe("isArray method:", function() {
        it("should detect Array values", function(){
            assert.isFalse((Types.isArray("string"))             , "Checking string");
            assert.isFalse((Types.isArray(""))                   , "Checking empty string");
            assert.isFalse((Types.isArray(true))                 , "Checking true");
            assert.isFalse((Types.isArray(false))                , "Checking false");
            assert.isFalse((Types.isArray(3.14))                 , "Checking decimal");
            assert.isFalse((Types.isArray(NaN))                  , "Checking NaN");
            assert.isFalse((Types.isArray(null))                 , "Checking null");
            assert.isFalse((Types.isArray(undefined))            , "Checking undefined");
            assert.isFalse((Types.isArray())                     , "Checking missing argument");
            assert.isFalse((Types.isArray({}))                   , "Checking empty object");
            assert.isFalse((Types.isArray(function(){}))         , "Checking function");
            assert.isTrue ((Types.isArray([]))                   , "Checking empty array");
        });
    });

    describe("isFunction method:", function() {
        it("should detect Function values", function(){
            assert.isFalse((Types.isFunction("string"))              , "Checking string");
            assert.isFalse((Types.isFunction(""))                    , "Checking empty string");
            assert.isFalse((Types.isFunction(true))                  , "Checking true");
            assert.isFalse((Types.isFunction(false))                 , "Checking false");
            assert.isFalse((Types.isFunction(3.14))                  , "Checking decimal");
            assert.isFalse((Types.isFunction(NaN))                   , "Checking NaN");
            assert.isFalse((Types.isFunction(null))                  , "Checking null");
            assert.isFalse((Types.isFunction(undefined))             , "Checking undefined");
            assert.isFalse((Types.isFunction())                      , "Checking missing argument");
            assert.isFalse((Types.isFunction({}))                    , "Checking empty object");
            assert.isTrue ((Types.isFunction(function(){}))          , "Checking function");
            assert.isFalse((Types.isFunction([]))                    , "Checking empty array");
        });
    });

    describe("getType method:", function() {
        it("should return the right type", function(){
            assert.equal((Types.getType("string"))                ,"string"    , "Checking string");
            assert.equal((Types.getType(""))                      ,"string"    , "Checking empty string");
            assert.equal((Types.getType(true))                    ,"boolean"   , "Checking true");
            assert.equal((Types.getType(false))                   ,"boolean"   ,  "Checking false");
            assert.equal((Types.getType(3.14))                    ,"number"    , "Checking decimal");
            assert.equal((Types.getType(NaN))                     ,"NaN"       , "Checking NaN");
            assert.equal((Types.getType(null))                    ,"null"      , "Checking null");
            assert.equal((Types.getType(undefined))               ,"undefined" , "Checking undefined");
            assert.equal((Types.getType())                        ,"undefined" , "Checking missing");
            assert.equal((Types.getType({}))                      ,"object"    , "Checking empty object");
            assert.equal((Types.getType(function(){}))            ,"function"  , "Checking function");
            assert.equal((Types.getType([]))                      ,"array"     , "Checking empty array");
        });
    });
});

