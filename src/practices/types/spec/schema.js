var assert = chai.assert;
var expect = chai.expect;
mocha.setup({globals: ['setTimeout', 'setInterval', 'clearTimeout', 'clearInterval']});

describe("Types Practice 2,", function(){

  describe("JSONSchema", function() {

    describe("[]", function() {
      it("Detects an empty array", function() {
        var json = [];
        var schema = [];
        assert.isTrue(JSONSchema(json, schema));
      });
      it("Detects an array with any content", function() {
        var json = [1, 2, 3, true, true, false, 42];
        var schema = [];
        assert.isTrue(JSONSchema(json, schema));
      });
    });

    describe("[type]", function() {
      it("Detects an array of the same length", function() {
        var schema = ["number", "number", "number"];
        assert.isTrue (JSONSchema([1, 2, 3], schema));
        assert.isFalse(JSONSchema([1, 2], schema));
        assert.isFalse(JSONSchema([1, 2, 3, 4], schema));
      });
      it("Check for types", function() {
        var schema = ["number", "number", "number"];
        assert.isTrue  (JSONSchema([1, 2, 3], schema));
        assert.isFalse (JSONSchema(["1", 2, 3], schema), "Using string as first item");
        assert.isFalse (JSONSchema([true, 2, 3], schema), "Using boolean as first item");
      })
    });

    describe("Nested []", function() {
      it("Detects empty array inside array", function() {
        var schema = [ [] ];
        assert.isFalse(JSONSchema([1, 2], schema), "Not nested");
        assert.isTrue(JSONSchema([ [1, 2] ], schema), "Nested");
      });
      it("Detects typed array inside array", function() {
        var schema = [ ["number"] ];
        assert.isTrue(JSONSchema([[1]], schema), "Using number");
        assert.isFalse(JSONSchema([[true]], schema), "Using boolean");
        assert.isFalse(JSONSchema([["1"]], schema), "Using string");
      });
    });

    describe("[] madness", function() {
      it("3x3 matrix", function() {
        var schema = [ ["number", "number", "number"],["number", "number", "number"],["number", "number", "number"] ];
        assert.isTrue(JSONSchema([ [1,2,3], [4,5,6], [7,8,9] ], schema));
      });
      it("2x2x2 matrix", function() {
        var schema = [ [["number", "number"],["number", "number"]], [["number", "number"],["number", "number"]] ];
        assert.isTrue(JSONSchema([ [[1,2], [3,4]], [[5,6], [7,8]] ], schema));
      });
    });

    describe("{}", function() {
      it("Detects an empty object", function() {
        var json = {};
        var schema = {};
        assert.isTrue(JSONSchema(json, schema));
      });
      it("Detects object with any content", function() {
        var json = {a:1, b:true, c:"anything"};
        var schema = {};
        assert.isTrue(JSONSchema(json, schema));
      });
    });

    describe("{property: type}", function() {
      it("Fails if the object has extra attributes", function() {
        var schema = {a: "number"};
        assert.isTrue(JSONSchema({a:1}, schema));
        assert.isFalse(JSONSchema({a:1, b:2}, schema));
      });
      it("Check for scalar types", function() {
        var schema = {a: "boolean", b: "string"};

        assert.isFalse(JSONSchema({a:1, b:2}, schema));
        assert.isFalse(JSONSchema({a:"1", b:2}, schema));
        assert.isFalse(JSONSchema({a:false, b:2}, schema));

        assert.isFalse(JSONSchema({a:1, b:2}, schema));
        assert.isFalse(JSONSchema({a:1, b:"2"}, schema));
        assert.isFalse(JSONSchema({a:1, b:true}, schema));

        assert.isTrue(JSONSchema({a:false, b:"name"}, schema));
      });
    });

    describe("Nested {}", function(){
      it("Detects a typed object inside an object", function() {
        var schema = { obj: { a: "number"} };
        assert.isFalse(JSONSchema({a: 3 }, schema));
        assert.isTrue(JSONSchema({obj: {a: 3} }, schema));
      });
    });

    describe("{} madness", function(){
      it("Detects lots of nested objects", function() {
        var schema = {a:{b:{c:{d:{e:{}}}}}};
        assert.isTrue(JSONSchema({a:{b:{c:{d:{e:{f:"test"}}}}}}, schema), "Lots of nesting");
        assert.isTrue(JSONSchema(schema, schema), "This schema validates itself");
      });
    });

    describe("Nesting!", function() {
      it("Detects arrays inside objects", function() {
        var schema = { a: [] };
        assert.isTrue(JSONSchema({a:[1,2,3]}, schema));
      });
      it("Detects objects inside arrays", function() {
        var schema = [ {}, {a:"number"}];
        assert.isTrue(JSONSchema([ {b:2}, {a:1} ] ,schema));
      });
      it("Detects everything inside everything", function() {
        var schema = [
          "number",
          "string",
          "boolean",
          {
            n: "number",
            s: "string",
            b: "boolean",
            a: [
              "number",
              "string",
              "boolean"
            ],
            o: {
              n: "number",
              s: "string",
              b: "boolean"
            }
          },
          [
            "number",
            "string",
            "boolean",
            {
              n: "number",
              s: "string",
              b: "boolean"
            }
          ]
        ];

        assert.isTrue(JSONSchema(
          [1, "2", false,
            { n: 1, s: "2", b:false, a:[1, "2", false], o:{n: 1, s: "2", b:false} },
            [ 1, "2", false, {n: 1, s: "2", b:false} ]
          ], schema));
      });

    });

    describe("Are you recursing properly?", function() {
      it("Validates arrays inside objects and items afterwards", function() {
        var schema = { a: [], b: "string" };
        assert.isFalse(JSONSchema({a:[1,2,3], b: 1234}, schema));
        assert.isTrue(JSONSchema({a:[1,2,3], b: "hello"}, schema));
      });
    });
  });


});
