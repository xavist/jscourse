/**
 * Implement an object called "Types" with the following methods
 *
 *   .isString(val): Checks if a value is a string
 *   .isBoolean(val): Checks if a value is a boolean
 *   .isNumber(val): Checks if a value is a number and not NaN
 *   .isNaN(val): Checks if a value is NaN
 *   .isNull(val): Checks if a value is null
 *   .isUndefined(val): Checks if a value is undefined
 *   .isObject(val): Checks if a value is an object (and not an Array, a Function, etc)
 *   .isArray(val): Checks if a value is an Array
 *   .isFunction(val): Checks if a value is a function
 *   .getType(val): Returns the type as a string ("string", "boolean", "number", "NaN", "undefined", "null", "function",
 *     "object", "array")
 *
 *
 * Implements your methods as object properties. Example:
 *
 *   Types = {
 *     isString: function() {
 *       //your amazing implementation here
 *     },
 *     isBoolean: function() {
 *       //your amazing implementation here
 *     }
 *     ...
 *   }
 *
 * Tips:
 *   - Read the tests!! Some definitions might not match the "native" definitions
 *
 *   - For extra points, try to implement `getType()` without calling the other methods. Ask yourself this question:
 *     "For what values the native `typeof` is already correct?"
 */

Types = {
};
