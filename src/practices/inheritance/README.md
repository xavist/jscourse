Atlassian Inheritance
=====================

You have to implement an inheritance library called `Class`. This is the API of the library:

Define a new class
-------------------
We want to define the class methods as an object, like this:

  var MyClass = Class({
    method1: function() {
      //...  
    },
    method2: function() {
      //...
    },
    //...
  })

Instantiate a class
-------------------
To instantiate a class, we want to do

  var instance = new MyClass()


The constructor
---------------
The constructor will be a regular method called `constructor`. Example:

  var Person = Class({
    constructor: function(name) {
      this.name = name;
    },
    sayYourName: function() {
      console.log(this.name);
    }
  })
  var tom = new Person("Tom");
  tom.sayYourName(); //"Tom"


Extending a class
-----------------
We want to extend existing classes like this:

  var Child = Class(Parent, {
    // Methods for class Child here
  })


Calling the parent
------------------

We want to call the parent using `this.super()`, like this:

  var Parent = Class({
    getValue: function(value) {
      return value + 2;
    }
  });

  var Child = Class(Parent,{
    getValue: function() {
      return this.super(3) + 1;
    }
  });

  var c = new Child();
  c.getValue() //6 (3 + 2 + 1);


Otional arguments
-----------------

All the arguments are optional. Example:

  //All of these are valid syntax
  var class1 = Class()                      // Create an empty class, extending from Object
  var class2 = Class({/*methods*/})         // Create a class with the specified methods
  var class3 = Class(Parent)                // Create an empty class, extending from Parent
  var class4 = Class(Parent, {/*methods*/}) // Create a class with the specified methods, extending from Parent

  //Tip:
  // In Class(Parent), the first argument is a function.
  // In Class({/*methods*/}), the first argument is an object.


