function Class(parentClass, classDefinition) {
    var parent;
    var definition;

    if (arguments.length === 0) {
        parent = Object;
        definition = {};
    } else if (arguments.length === 1) {
        if (typeof arguments[0] == "function") {
            parent = arguments[0];
            definition = {};
        } else if (typeof arguments[0] == "object") {
            parent = Object;
            definition = arguments[0]
        }
    } else if (arguments.length === 2) {
        parent = parentClass;
        definition = classDefinition;
    }

    var prototype = Object.create(parent.prototype);
    for (var i in definition) {
        prototype[i] = (function(i) {
            return function() {
                this.super = function() {
                    return parent.prototype[i].apply(this, arguments);
                }
                return definition[i].apply(this,arguments);
            }
        })(i)
    }

    var f = function() {
        prototype.constructor.apply(this, arguments)
    };
    f.prototype = prototype;
    return f;
}