var expect = chai.expect;
mocha.setup({globals: ['setTimeout', 'setInterval', 'clearTimeout', 'clearInterval']});

describe("Class library,", function(){

    describe("Basic 'Class' API", function() {
        it("should be defined as a function", function() {
            expect(Class).to.be.a("function");
        });

        it("should return a function function", function() {
            expect(Class()).to.be.a("function");
        });
    });

    describe("Class instantiation", function() {
        it("should create an object", function() {
            var MyClass = Class();
            var instance = new MyClass();

            expect(instance).to.be.an("object");
        });
        it("'instanceof' should work", function() {
            var MyClass = Class();
            var instance = new MyClass();

            expect(instance instanceof MyClass).to.be.equal(true);
        });
    });

    describe("Methods", function() {
        it("can be defined when creating the class", function() {
            var MyClass = Class({
                myMethod: function() {return true;}
            })
            var instance = new MyClass();

            expect(instance.myMethod).to.be.a('function');
            expect(instance.myMethod()).to.be.equal(true)
        });
    });

    describe("Inheritance", function() {
        it("get the methods from the parent class", function() {
            var Parent = Class({
                parentMethod: function() {return true}
            })
            var Child = Class(Parent, {
                childMethod: function() {return true}
            })
            var instance = new Child();

            expect(instance.parentMethod).to.be.a('function');
            expect(instance.parentMethod()).to.be.equal(true);
        });

        it("does not share methods with other instances", function() {
            var Parent = Class()
            var Child1 = Class(Parent, {
                getName: function() { return 'child1' }
            });
            var Child2 = Class(Parent, {
                getName: function() { return 'child2' }
            });

            var i1 = new Child1();
            var i2 = new Child2();
            expect(i1.getName()).to.be.equal('child1');
            expect(i2.getName()).to.be.equal('child2');
        });
    });

    describe("Constructor", function(){
        it("executes the constructor when creating the instance", function() {
            var MyClass = Class({
                constructor: function() {
                    this.constructed = true;
                }
            });

            var instance = new MyClass();
            expect(instance.constructed).to.be.equal(true);
        });
        it("passes the arguments to the constructor", function() {
            var Sum = Class({
                constructor: function(a,b,c,d) {
                    this.total = a+b+c+d;
                }
            });

            var instance = new Sum(1,2,3,4);
            expect(instance.total).to.be.equal(10);
        });
        it("have a separate constructor for each instance", function() {
            var MyClass = Class({
                constructor: function(name) {
                    this.name = name;
                }
            });

            var instance1 = new MyClass("name1");
            var instance2 = new MyClass("name2");

            expect(instance1.name).to.be.equal("name1");
            expect(instance2.name).to.be.equal("name2");
        });
        it("calls the parent constructor if the child class does not have one", function() {
            var Parent = Class({
                constructor: function(name) {
                    this.name = name;
                }
            });
            var Child = Class(Parent);

            var instance = new Child("rachel");
            expect(instance.name).to.be.equal("rachel");
        });
    });

    describe("Overriding methods", function() {
        it("overrides methods from the parent class", function() {
            var Parent = Class({
                getName: function() {return "Parent"}
            })
            var Child = Class(Parent, {
              getName: function() {return "Child"}
            })

            var parentInstance = new Parent();
            var childInstance = new Child();
            expect(parentInstance.getName()).to.be.equal('Parent');
            expect(childInstance.getName()).to.be.equal('Child');
        });
        it("allows usage of super() to call the parent method", function() {
            var Parent = Class({
                getName: function() {return "Parent"}
            })
            var Child = Class(Parent, {
                getName: function() {return this.super().toUpperCase(); }
            })

            var childInstance = new Child();
            expect(childInstance.getName()).to.be.equal('PARENT');
        });
        it("allows passing arguments to super()", function() {
            var Parent = Class({
                getName: function() {return "Parent"}
            })
            var Child = Class(Parent, {
                getName: function() {return this.super().toUpperCase(); }
            })

            var childInstance = new Child();
            expect(childInstance.getName()).to.be.equal('PARENT');
        });
    });
});
