(function (global) {

    global.Tabify = {
        start: function () {
            var collection = new TabCollection(document.getElementsByTagName("section"));
            collection.render();
            collection.activate(0);
        }
    }

    var TabCollection = function (elements) {
        //Initialize the colleciton of tabs
        this.tabs = [];

        //Tab() will consume the element from the collection
        while (el = elements[0]) {
            this.tabs.push(new Tab(el));
        }
    }

    TabCollection.prototype.render = function () {
        this.renderTriggers();
        this.renderContent();
    };

    TabCollection.prototype.renderTriggers = function () {
        var instance = this;

        //Create the UL element
        var ul = document.createElement("ul");
        ul.className = "tabs";
        ul.addEventListener("click", function (ev) {
            var trigger = ev.target;
            for (var i = 0; i < instance.tabs.length; i++) {
                var tab = instance.tabs[i];
                if (trigger == tab.trigger) {
                    instance.activate(i);
                    break;
                }
            }
        });

        //Add each tab trigger
        for (var i = 0; i < this.tabs.length; i++) {
            var tab = this.tabs[i];
            ul.appendChild(tab.trigger);
        }

        document.body.appendChild(ul);
    };

    TabCollection.prototype.renderContent = function () {
        //Create the container
        var div = document.createElement("div");
        div.className = "content";

        //Pass a reference to the container to each tab
        for (var i = 0; i < this.tabs.length; i++) {
            this.tabs[i].container = div;
        }

        document.body.appendChild(div);
    }

    TabCollection.prototype.activate = function (index) {
        for (var i = 0; i < this.tabs.length; i++) {
            var tab = this.tabs[i];
            if (index == i) {
                tab.activate();
            } else {
                tab.deactivate();
            }
        }
    }

    var Tab = function (el) {
        //Remove ourselves from the document tree
        el.parentNode.removeChild(el);

        this._extractTitle(el);
        this._extractContent(el);
        this._generateTrigger(el);
    }

    Tab.prototype._extractTitle = function (el) {
        var titleContainer = el.getElementsByTagName("h1")[0];
        this.title = titleContainer.innerText;
        el.removeChild(titleContainer);
    }

    Tab.prototype._extractContent = function (el) {
        this.content = Array.prototype.slice.apply(el.children, [0]);
    }

    Tab.prototype._generateTrigger = function () {
        var li = document.createElement("li");
        li.appendChild(document.createTextNode(this.title));
        li.className = "tab";
        this.trigger = li;
    }

    Tab.prototype.activate = function () {
        this.trigger.className = "active";

        //Clear the container
        while (el = this.container.children[0]) {
            this.container.removeChild(el);
        }

        //Append our content to the container
        for (var i = 0; i < this.content.length; i++) {
            this.container.appendChild(this.content[i]);
        }
    }

    Tab.prototype.deactivate = function () {
        this.trigger.className = "";
    }

})(window);