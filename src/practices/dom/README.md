We have a HTML document with the following format (index.html):

```
<html>
<body>
    <section>
        <h1>Tab title - 1</h1>
        <p>Content 1</p>
    </section>

    <section>
        <h1>Tab title - 2</h1>
        <p>Content 2</p>
        <p>Content 2</p>
    </section>

</body>
</html>
```

We want to transform this static document into a Tab panel, using JavaScript. When the user clicks on a Tab title,
the content should be displayed inside the DIV.

You need to implement a library called 'Tabify'. When the document is loaded, we are calling to 'Tabify.start()'.
Use that method as your entry point.

Rules:

- Your solution should be generic, must work with any number of tabs.
- You can't pollute the global scope with extra functions or objects. Only the 'Tabify' object is allowed.
- You are not allowed to use CSS. All the required CSS is already provided.
- Try to minimize the DOM lookups.
- Don't store content as string or objects, use DOM Elements.
- The active Tab must have the class "active"
- Obviously, only one Tab can be active at the same time.
- The final HTML structure should be (don't generate the comments):

```
<html>
<body>

    <ul class="tabs">
        <li class="">Tab title - 1</li>
        <li class="">Tab title - 2</li>
        <li class="active">Tab title - 3</li>  <!-- Active tab -->
    </ul>

    <div class="content">
        <!-- Active tab content here -->
        <p>Content 3</p><p>Content 3</p>
    </div>

</body>
</html>
```