Implement an object called "Functional" with the following methods.

NOTE: THESE ARE VERY BASIC EXAMPLES, CHECK THE SPEC FOR THE FULL IMPLEMENTATION

* bind(fn, object)
   Locks the value of `this` inside a function. Example:

   var f1 = function() { return this }
   var testObject = {};
   var f2 = Functional.bind(f1, testObject);
   f2() === testObject


* partial(fn, param...)
   Partially apply arguments to a function. Example:

   var f1 = function(a,b) {return a+b}
   var f2 = Functional.partial(f1, 10)
   f2(5) === 15
   f2(0) === 10


* curry(fn)
   Transforms a function like fn(a,b,c) to a function in the form fn(a)(b)(c). Example:

   var f1 = function(a,b,c) {return a+b+c}
   var f2 = Functional.curry(f1)
   f1(10,5,1) === f2(10)(5)(1) === 16


* intercept(fn, interceptor)
   Runs a function before the original function, run the original function if the interceptor returns
   true. Example:

   var f1 = function(a) {alert(a)}
   var isOne = function(a) {return a===1}
   var f2 = Functional.intercept(f1, isOne)
   f2(1) //alerts 1
   f2(2) //does nothing


* append(fn, append)
   Runs a function after the original function, receiving the output of the original function. Example:

   var f1 = function() { return "name" }
   var toUpperCase = function(a) { return a.toUpperCase() }
   var f2 = Functional.append(f1, toUpperCase);
   f2() === "NAME"


* adapt(fn, adapter)
   Modifies the arguments passed to a function. Example:

   var f1 = function(a,b) { return "Hello " + a + b }
   var reverse = function(a,b) { return [b,a] }
   var f2 = Functional.append(f1, reverse);
   f1("mr","charlie") === "Hello mr charlie"
   f2("mr","charlie") === "Hello charlie mr"


* mandatory(fn)
   Validates the number of arguments passed to a function. Example:

   var f1 = function(a,b) { return a+b}
   var f2 = Functional.mandatory(f1);
   f1(1,2)    //3
   f2(1,2,3) //Error


* reset()
   Reset the last Functional transformation applied to a function. Example:

   var f1 = function() { return true }
   var f2 = Functional.mandatory(f1);
   var f3 = Functional.reset(f2);
   f1 === f3


* resetAll()
   Reset all Functional transformations applied to a function

   var f1 = function() { return true }
   var f2 = Functional.mandatory(f1);
   var f3 = Functional.curry(f2);
   var f4 = Functional.resetAll()
   f1 === f4

