var expect = chai.expect;
mocha.setup({globals: ['setTimeout', 'setInterval', 'clearTimeout', 'clearInterval']});

describe("Functional utils,", function(){

    describe("bind method:", function() {

        beforeEach(function(){
            //No easy cheats :)
            Function.prototype.bind = null;
        });

        it("should be exposed as a function", function(){
            expect(Functional.bind).to.be.a("function");
        });

        it("should create a new function", function(){
            var fn = function(){};

            var result = Functional.bind(fn);

            expect(result).to.be.not.equal(fn);
            expect(result).to.be.a("function");
        });

        it("should preseve the result of the original function", function(){
            var fn = function(){return "test"};

            var result = Functional.bind(fn);

            expect(result()).to.be.equal("test");
        });

        it("should bind an object to a function", function(){
            var fn = function(){return this.name};
            var obj = {name: "name"};

            var result = Functional.bind(fn, obj);

            expect(result()).to.be.equal("name");
        });

        it("should not re-bind an already bound function", function(){
            var fn = function(){return this.name};
            var obj1 = {name: "name1"};
            var obj2 = {name: "name2"};

            var result1 = Functional.bind(fn, obj1);
            var result2 = Functional.bind(result1, obj2);

            expect(result2()).to.be.equal("name1");
        });

        it("should pass arguments to the bound function", function(){
            var fn = function(a,b,c){return [a,b,c]};
            var obj = {name: "name"};

            var result = Functional.bind(fn, obj);

            expect(result(1,2,3)).to.be.deep.equal([1,2,3]);
        });

    });

    describe("partial method:", function(){

        it("should be exposed as a function", function(){
            expect(Functional.partial).to.be.a("function");
        });

        it("should create a new function", function(){
            var fn = function(){};

            var result = Functional.partial(fn);

            expect(result).to.be.not.equal(fn);
            expect(result).to.be.a("function");
        });

        it("should preserve the result of the original function", function(){
            var fn = function(){return "test"};

            var result = Functional.partial(fn);

            expect(result()).to.be.equal("test");
        });

        it("should pass arguments to the original function", function(){
            var fn = function(a,b){return a+b};

            var result = Functional.partial(fn);

            expect(result(1,2)).to.be.equal(3);
        });

        it("should partially apply arguments to the original function", function(){
            var fn = function(a,b){return a+b};

            var result = Functional.partial(fn, 1);

            expect(result(2)).to.be.equal(3);
        });

        it("should support multiple partial applications", function(){
            var fn = function(a,b){return a+b};

            var result1 = Functional.partial(fn, 1);
            var result2 = Functional.partial(result1, 2);

            expect(result2()).to.be.equal(3);
        });

    });

    describe("curry method:", function() {

        it("should be exposed as a function", function(){
            expect(Functional.curry).to.be.a("function");
        });

        it("should create a new function", function(){
            var fn = function(){};

            var result = Functional.curry(fn);

            expect(result).to.be.not.equal(fn);
            expect(result).to.be.a("function");
        });

        it("should preserve the result of the original function", function(){
            var fn = function(){return "test"};

            var result = Functional.curry(fn);

            expect(result()).to.be.equal("test");
        });

        it("should work like the original function when only one argument is used", function(){
            var fn = function(a){return a};

            var result = Functional.curry(fn);

            expect(result(1)).to.be.equal(fn(1));
        });

        it("should work with multiple arguments", function(){
            var fn = function(a,b,c){return a+b+c};

            var result = Functional.curry(fn);

            expect(result(1)(2)(3)).to.be.equal(fn(1,2,3));
        });

    });

    describe("intercept method:", function() {

        it("should be exposed as a function", function(){
            expect(Functional.intercept).to.be.a("function");
        });

        it("should create a new function", function(){
            var fn = function(){};
            var intercept = function(){};

            var result = Functional.intercept(fn, intercept);

            expect(result).to.be.not.equal(fn);
            expect(result).to.be.a("function");
        });

        it("should pass arguments to the original function", function(){
            var fn = function(a,b){return a+b};
            var intercept = function(){ return true };

            var result = Functional.intercept(fn, intercept);

            expect(result(1,2)).to.be.equal(3);
        });

        it("should not run original function if intercept returns false", function(){
            var fn = function(a,b){return a+b};
            var intercept = function(){ return false };

            var result = Functional.intercept(fn, intercept);

            expect(result(1,2)).to.be.an("undefined");
        });

        it("should pass arguments to the intercept function", function(){
            var fn = function(a,b){return a+b};
            var intercept = function(a,b){ return (a+b == 3) };

            var result = Functional.intercept(fn, intercept);

            expect(result(1,2)).to.be.equal(3);
        });
    });

    describe("append method:", function() {

        it("should be exposed as a function", function(){
            expect(Functional.append).to.be.a("function");
        });

        it("should create a new function", function(){
            var fn = function(){};
            var append = function(){};

            var result = Functional.append(fn, append);

            expect(result).to.be.not.equal(fn);
            expect(result).to.be.a("function");
        });

        it("append should receive the result of the original function", function(){
            var fn = function(){return "test"};
            var append = function(result){ return result.toUpperCase() };

            var result = Functional.append(fn, append);

            expect(result()).to.be.equal("TEST");
        });

        it("should pass arguments to the original function", function(){
            var fn = function(a,b){return a+b};
            var append = function(result){ return result };

            var result = Functional.append(fn, append);

            expect(result(1,2)).to.be.equal(3);
        });

        it("should pass arguments to the append function", function(){
            var fn = function(a,b){return a+b};
            var append = function(result, args){ return result + args[0] };

            var result = Functional.append(fn, append);

            expect(result(1,2)).to.be.equal(4);
        });

    });

    describe("adapt method:", function() {

        it("should be exposed as a function", function(){
            expect(Functional.adapt).to.be.a("function");
        });

        it("should create a new function", function(){
            var fn = function(){};
            var adapter = function(){};

            var result = Functional.adapt(fn, adapter);

            expect(result).to.be.not.equal(fn);
            expect(result).to.be.a("function");
        });

        it("should pass arguments to the adapter function", sinon.test(function(){
            var fn = this.spy();
            var adapter = this.spy();

            Functional.adapt(fn, adapter)(1,2);

            expect(adapter.calledOnce).to.be.true;
            expect(adapter.firstCall.args).to.be.deep.equal([1,2]);
        }));

        it("should pass arguments to the original function", sinon.test(function(){
            var fn = this.spy();
            var adapter = function(a,b){return [a,b]};

            Functional.adapt(fn, adapter)(1,2);

            expect(fn.calledOnce).to.be.true;
            expect(fn.firstCall.args).to.be.deep.equal([1,2]);
        }));

        it("should pass the response from adapter as original function arguments", sinon.test(function(){
            var fn = function(a,b){return a+b};
            var adapter = function(a,b){return [a*2,b*2]};

            var result = Functional.adapt(fn, adapter);

            expect(result(1,2)).to.be.equal(6);
        }));

    });

    describe("mandatory method:", function(){

        it("should be exposed as a function", function(){
            expect(Functional.mandatory).to.be.a("function");
        });

        it("should create a new function", function(){
            var fn = function(){};

            var result = Functional.mandatory(fn);

            expect(result).to.be.not.equal(fn);
            expect(result).to.be.a("function");
        });

        it("should pass arguments to the original function", function(){
            var spy = sinon.spy(function(a,b,c){ return a+b+c });

            Functional.mandatory(spy)(1,2,3);

            expect(spy.calledOnce).to.be.true;
            expect(spy.firstCall.args).to.be.deep.equal([1,2,3])
        });

        it("should return the response from the original function", function(){
            var fn = function(a,b){return a+b};

            var result = Functional.mandatory(fn);

            expect(result(1,2)).to.be.equal(3);
        });

        it("should fail if number of expected arguments is not met", function(){
            var fn = function(a,b){return a+b};

            var resultFn = Functional.mandatory(fn);

            expect(resultFn).to.throw(Error);
        });

    });

    describe("reset method:", function(){

        it("should be exposed as a function", function(){
            expect(Functional.reset).to.be.a("function");
        });

        it("should reset last bind transformation", function(){
            var fn = function(a,b){return a+b};

            var result = Functional.reset(Functional.bind(fn));

            expect(result).to.be.equal(fn);
        });

        it("should reset last partial transformation", function(){
            var fn = function(a,b){return a+b};

            var result = Functional.reset(Functional.partial(fn));

            expect(result).to.be.equal(fn);
        });

        it("should reset last curry transformation", function(){
            var fn = function(a,b){return a+b};

            var result = Functional.reset(Functional.curry(fn));

            expect(result).to.be.equal(fn);
        });

        it("should reset last intercept transformation", function(){
            var fn = function(a,b){return a+b};

            var result = Functional.reset(Functional.intercept(fn, function(){}));

            expect(result).to.be.equal(fn);
        });

        it("should reset last append transformation", function(){
            var fn = function(a,b){return a+b};

            var result = Functional.reset(Functional.append(fn, function(){}));

            expect(result).to.be.equal(fn);
        });

        it("should reset last adapt transformation", function(){
            var fn = function(a,b){return a+b};

            var result = Functional.reset(Functional.adapt(fn, function(){}));

            expect(result).to.be.equal(fn);
        });

        it("should reset last mandatory transformation", function(){
            var fn = function(a,b){return a+b};

            var result = Functional.reset(Functional.mandatory(fn));

            expect(result).to.be.equal(fn);
        });
    });

    describe("resetAll method:", function(){

        it("should be exposed as a function", function(){
            expect(Functional.resetAll).to.be.a("function");
        });

        it("should reset all transformations", function(){
            var fn = function(a,b){return a+b};

            var result = Functional.resetAll(
                Functional.bind(
                    Functional.mandatory(
                        Functional.mandatory(
                            Functional.adapt(fn, function(){})
                        )
                    )
                )
            );

            expect(result).to.be.equal(fn);
        });

    });
});