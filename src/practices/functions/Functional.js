/*
 * Implements your methods as object properties. Example
 *
 * Functional = {
 *    bind: function() {
 *      //your amazing implementation
 *    },
 *    partial: function() {
 *      //your amazing implementation
 *    }
 *    ...
 * }
 *
 * TIPS that you might need:
 *
 *    - Join two arrays:
 *        var result = array1.concat(array2);  //It does not change array1
 *
 *    - Remove the first item from an array:
 *        var removedItem = array.shift();     //It changes array
 *
 */
