Functional = {

    bind: function(fn, obj) {
        var f = function() {
            return fn.apply(obj, arguments);
        };
        f._originalFn = fn;
        return f;
    },

    partial: function(fn){

        var originalArgs = [];
        for (var i = 1; i<arguments.length; i++) {
            originalArgs[i-1] = (arguments[i]);
        }

        var f = function() {
            var newArgs = [];
            for (var i = 0; i<arguments.length; i++) {
                newArgs.push(arguments[i]);
            }

            return fn.apply(null, originalArgs.concat(newArgs));
        };
        f._originalFn = fn;
        return f;
    },

    curry: function(fn) {
        var savedArgs = [];

        var result = function(arg) {
            if (typeof arg !== "undefined") {
                savedArgs.push(arg);
            }

            if (savedArgs.length == fn.length) {
                return fn.apply(null, savedArgs);
            } else {
                return result;
            }
        };
        result._originalFn = fn;

        return result;
    },

    intercept: function(fn, guard) {
        var f = function() {
            var result = guard.apply(null, arguments);
            if (result) {
                return fn.apply(null, arguments);
            }
        };
        f._originalFn = fn;
        return f;
    },

    append: function(fn, append) {
        var f = function() {
            var originalArgs = [];
            for (var i = 0; i<arguments.length; i++) {
                originalArgs.push(arguments[i]);
            }

            var result = fn.apply(null, originalArgs);
            return append(result, originalArgs);
        };
        f._originalFn = fn;
        return f;
    },

    adapt: function(fn, adapter) {
        var f = function() {
            var originalArgs = [];
            for (var i = 0; i<arguments.length; i++) {
                originalArgs.push(arguments[i]);
            }

            return fn.apply(null, adapter.apply(null, originalArgs));
        };
        f._originalFn = fn;
        return f;
    },

    mandatory: function(fn) {
        var f = function() {
            if (arguments.length !== fn.length) {
                throw new Error("Incorrect number of arguments");
            }
            return fn.apply(null, arguments);
        };
        f._originalFn = fn;
        return f;
    },

    reset: function(fn) {
        return fn._originalFn;
    },

    resetAll: function(fn) {
        var originalFn;
        while (originalFn = fn._originalFn) {
            fn = originalFn;
        }
        return fn;
    }
};